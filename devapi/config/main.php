<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-devapi',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'devapi\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'timeZone' => 'Asia/Jakarta',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-devapi',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
            'loginUrl' => null,
            'identityClass' => 'devapi\models\User',
            'enableAutoLogin' => false,
            'identityCookie' => ['name' => '_identity-devapi', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-devapi',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['sts', 'lkpp', 'hasil', 'rekening-skpd'],
                    'pluralize' => false,
                    'extraPatterns' => [
                        'GET program' => 'program',
                        'GET kegiatan' => 'kegiatan',
                        'GET test' => 'test',
                        'POST create' => 'create',
                        'POST update' => 'update-sts',
                        'POST create-kontra' => 'create-kontra',
                        'POST update-kontra' => 'update-kontra-sts',
                        'POST tarik-data' => 'tarik-data'
                    ],
                ],
            ],
        ]

    ],
    'params' => $params,
];
