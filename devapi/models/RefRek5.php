<?php

namespace devapi\models;

use Yii;

/**
 * This is the model class for table "Ref_Rek_5".
 *
 * @property int $Tahun
 * @property int $Kd_Kab
 * @property int $Kd_Rek_1
 * @property int $Kd_Rek_2
 * @property int $Kd_Rek_3
 * @property int $Kd_Rek_4
 * @property int $Kd_Rek_5
 * @property string $Nm_Rek_5
 * @property string $Peraturan
 */
class RefRek5 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Rek_5';
    }

    public static function getDb(){
        return \Yii::$app->db_dev;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Kab', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'Nm_Rek_5'], 'required'],
            [['Tahun', 'Kd_Kab', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5'], 'integer'],
            [['Nm_Rek_5', 'Peraturan'], 'string'],
            [['Kd_Kab', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'Tahun'], 'unique', 'targetAttribute' => ['Kd_Kab', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Kab' => 'Kd  Kab',
            'Kd_Rek_1' => 'Kd  Rek 1',
            'Kd_Rek_2' => 'Kd  Rek 2',
            'Kd_Rek_3' => 'Kd  Rek 3',
            'Kd_Rek_4' => 'Kd  Rek 4',
            'Kd_Rek_5' => 'Kd  Rek 5',
            'Nm_Rek_5' => 'Nm  Rek 5',
            'Peraturan' => 'Peraturan',
        ];
    }
}
