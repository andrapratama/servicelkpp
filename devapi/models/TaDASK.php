<?php

namespace devapi\models;

use Yii;

/**
 * This is the model class for table "Ta_DASK".
 *
 * @property int $Tahun
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property string $No_DPA
 * @property string $Tgl_DPA
 * @property string $No_DPPA
 * @property string $Tgl_DPPA
 */
class TaDASK extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_DASK';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub'], 'required'],
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub'], 'integer'],
            [['No_DPA', 'No_DPPA'], 'string'],
            [['Tgl_DPA', 'Tgl_DPPA'], 'safe'],
            [['Kd_Bidang', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'Tahun'], 'unique', 'targetAttribute' => ['Kd_Bidang', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'No_DPA' => 'No  Dpa',
            'Tgl_DPA' => 'Tgl  Dpa',
            'No_DPPA' => 'No  Dppa',
            'Tgl_DPPA' => 'Tgl  Dppa',
        ];
    }
}
