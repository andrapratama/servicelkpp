<?php

namespace devapi\models;

use Yii;

/**
 * This is the model class for table "STS_History".
 *
 * @property int $Kd_Masuk
 * @property int $Kd_Tarik
 * @property int $Kd_Unit
 * @property string $No_STS
 * @property string $Tgl_STS
 * @property string $No_NOP
 * @property string $No_Pokok_WP
 * @property string $No_Pokok_WR
 * @property string $Nama_Pemilik
 * @property string $Alamat_Pemilik
 * @property string $Jn_Pajak
 * @property string $Nm_Pajak
 * @property string $Jn_Retribusi
 * @property string $Nm_Retribusi
 * @property string $Mata_Anggaran
 * @property string $No_Ketetapan
 * @property string $No_SSPD
 * @property string $Tgl_SSPD
 * @property string $No_STPD
 * @property string $Tgl_STPD
 * @property string $No_STRD
 * @property string $Tgl_STRD
 * @property string $Nilai
 * @property string $Denda
 * @property string $masa_bayar
 * @property string $Tgl_Bayar
 * @property int $Status_Bayar
 * @property string $Kode_Pengesahan
 * @property string $Kode_Cab
 * @property string $Nama_Channel
 * @property string $kode_terminal
 * @property string $jatuh_tempo
 * @property string $access_token
 * @property int $Pembayaran_Ke
 * @property int $Tahun
 */
class STSHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'STS_History';
    }

    public static function getDb(){
        return \Yii::$app->db_payment_dev;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Tarik', 'Nama_Pemilik', 'Alamat_Pemilik', 'Nilai', 'Denda'], 'required'],
            [['Kd_Tarik', 'Kd_Unit', 'Status_Bayar', 'Pembayaran_Ke', 'Tahun'], 'integer'],
            [['No_STS', 'No_NOP', 'No_Pokok_WP', 'No_Pokok_WR', 'Nama_Pemilik', 'Alamat_Pemilik', 'Jn_Pajak', 'Nm_Pajak', 'Jn_Retribusi', 'Nm_Retribusi', 'Mata_Anggaran', 'No_Ketetapan', 'No_SSPD', 'No_STPD', 'No_STRD', 'masa_bayar', 'Kode_Pengesahan', 'Kode_Cab', 'Nama_Channel', 'kode_terminal', 'access_token'], 'string'],
            [['Tgl_STS', 'Tgl_SSPD', 'Tgl_STPD', 'Tgl_STRD', 'Tgl_Bayar', 'jatuh_tempo'], 'safe'],
            [['Nilai', 'Denda'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Masuk' => 'Kd  Masuk',
            'Kd_Tarik' => 'Kd  Tarik',
            'Kd_Unit' => 'Kd  Unit',
            'No_STS' => 'No  Sts',
            'Tgl_STS' => 'Tgl  Sts',
            'No_NOP' => 'No  Nop',
            'No_Pokok_WP' => 'No  Pokok  Wp',
            'No_Pokok_WR' => 'No  Pokok  Wr',
            'Nama_Pemilik' => 'Nama  Pemilik',
            'Alamat_Pemilik' => 'Alamat  Pemilik',
            'Jn_Pajak' => 'Jn  Pajak',
            'Nm_Pajak' => 'Nm  Pajak',
            'Jn_Retribusi' => 'Jn  Retribusi',
            'Nm_Retribusi' => 'Nm  Retribusi',
            'Mata_Anggaran' => 'Mata  Anggaran',
            'No_Ketetapan' => 'No  Ketetapan',
            'No_SSPD' => 'No  Sspd',
            'Tgl_SSPD' => 'Tgl  Sspd',
            'No_STPD' => 'No  Stpd',
            'Tgl_STPD' => 'Tgl  Stpd',
            'No_STRD' => 'No  Strd',
            'Tgl_STRD' => 'Tgl  Strd',
            'Nilai' => 'Nilai',
            'Denda' => 'Denda',
            'masa_bayar' => 'Masa Bayar',
            'Tgl_Bayar' => 'Tgl  Bayar',
            'Status_Bayar' => 'Status  Bayar',
            'Kode_Pengesahan' => 'Kode  Pengesahan',
            'Kode_Cab' => 'Kode  Cab',
            'Nama_Channel' => 'Nama  Channel',
            'kode_terminal' => 'Kode Terminal',
            'jatuh_tempo' => 'Jatuh Tempo',
            'access_token' => 'Access Token',
            'Pembayaran_Ke' => 'Pembayaran  Ke',
            'Tahun' => 'Tahun',
        ];
    }
}
