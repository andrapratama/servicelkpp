<?php

namespace devapi\models;

use Yii;

/**
 * This is the model class for table "Ref_Unit".
 *
 * @property int $Tahun
 * @property int $Kd_Kab
 * @property int $id_unit
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property string $Nm_Unit
 */
class RefUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Unit';
    }

    public static function getDb(){
        return \Yii::$app->db_dev;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Kab', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Nm_Unit'], 'required'],
            [['Tahun', 'Kd_Kab', 'id_unit', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit'], 'integer'],
            [['Nm_Unit'], 'string'],
            [['Kd_Bidang', 'Kd_Kab', 'Kd_Unit', 'Kd_Urusan', 'Tahun'], 'unique', 'targetAttribute' => ['Kd_Bidang', 'Kd_Kab', 'Kd_Unit', 'Kd_Urusan', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Kab' => 'Kd  Kab',
            'id_unit' => 'Id Unit',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Nm_Unit' => 'Nm  Unit',
        ];
    }
}
