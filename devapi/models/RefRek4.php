<?php

namespace devapi\models;

use Yii;

/**
 * This is the model class for table "Ref_Rek_4".
 *
 * @property int $Tahun
 * @property int $Kd_Kab
 * @property int $Kd_Rek_1
 * @property int $Kd_Rek_2
 * @property int $Kd_Rek_3
 * @property int $Kd_Rek_4
 * @property string $Nm_Rek_4
 */
class RefRek4 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Rek_4';
    }

    public static function getDb(){
        return \Yii::$app->db_dev;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Kab', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Nm_Rek_4'], 'required'],
            [['Tahun', 'Kd_Kab', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4'], 'integer'],
            [['Nm_Rek_4'], 'string'],
            [['Kd_Kab', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Tahun'], 'unique', 'targetAttribute' => ['Kd_Kab', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Kab' => 'Kd  Kab',
            'Kd_Rek_1' => 'Kd  Rek 1',
            'Kd_Rek_2' => 'Kd  Rek 2',
            'Kd_Rek_3' => 'Kd  Rek 3',
            'Kd_Rek_4' => 'Kd  Rek 4',
            'Nm_Rek_4' => 'Nm  Rek 4',
        ];
    }
}
