<?php

namespace devapi\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use devapi\models\RefRek4;
use devapi\models\RefRek5;
use common\models\TrxRekKoranSkpd;
use linslin\yii2\curl;

class RekeningSkpdController extends ActiveController
{
	public $modelClass = 'devapi\models\TrxRekKoranSkpd';

	public function actions() {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update'], $actions['view'], $actions['index']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        //$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actionTarikData(){
        $date1 = date('Y-m-d');
        $date2 = date('Ymd');
        $time1 = date('H:i:s');
        $time2 = date('His');
        $dateStart = $date1;
        $dateEnd = $date1;
        $model = new \yii\base\DynamicModel([
            'Tgl_Dari', 'Tgl_Sampai', 'accNbr'
        ]);
        $model->addRule(['Tgl_Dari','Tgl_Sampai'], 'safe');
        $model->addRule(['accNbr'],'integer');
        if($model->load(\Yii::$app->getRequest()->getBodyParams(), '')){
            $dateStart = $model->Tgl_Dari;
            $dateEnd = $model->Tgl_Sampai;
        }
        $arr_lama = TrxRekKoranSkpd::find()->where([
            'between', 'TXDATE', $dateStart, $dateEnd
        ])->asArray()->all();
        $authKey = hash_hmac('sha1', '00008'.'120.34.42.18'.$date1.$time1, 'emogw');
        $curl = new curl\Curl();
        $params = json_encode([
                'authKey' => $authKey,
                'reqId' => '00008',
                'txDate' => $date2,
                'txHour' => $time2,
                'userGtw' => 'emogw',
                'channelId' => '33',
                'accNbr' => $model->accNbr,
                'startDate' => $dateStart,
                'endDate' => $dateEnd
             ]);
        //echo $params;
        $response = $curl->setRequestBody($params)
             ->setHeaders([
                'Content-Type' => 'application/json',
                'Content-Length' => strlen($params)
             ])
             ->post('http://192.168.30.70:6565/Gateway/gateway/services/v2/postData');
        //echo "<pre>";
        $arr = json_decode($response, TRUE)['result'];
        //var_dump($params);
        //var_dump($params);exit;
        $arr_baru = array_udiff($arr, $arr_lama, [$this,'cek_perbedaan']);
        if (count($arr_baru) > 0){
            $model = new TrxRekKoranSkpd();
            for ($i = 0;$i < count($arr_baru);$i += 1000){
                $arr_temp = array_slice($arr_baru, $i, 1000);
                $arr_temp = $this->pembersihan($arr_temp);
                \Yii::$app->db_dev->createCommand()->batchInsert($model::tableName(), array_keys($arr_temp[0]), $arr_temp)->execute();
            }
        }
        return ['message' => "Data baru pada waktu ".$date1." ".$time1." adalah sebanyak ".count($arr_baru)."."];
    }

    private function cek_perbedaan($a, $b){
        $model = new TrxRekKoranSkpd();
        $model->attributes = $a;
        return $model->validate();
    }

    private function pembersihan($arr){
        $model = new TrxRekKoranSkpd();
        foreach ($arr as $value) {
            $arr_diff = array_diff(array_keys($value), $model->attributes());
            foreach ($arr_diff as $values) {
                unset($value[$values]);
            }
            $arr_temp[] = $value;
        }
        return $arr_temp;
    }
}
