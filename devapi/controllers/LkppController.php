<?php

namespace devapi\controllers;

use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class LkppController extends ActiveController
{
    public $modelClass = 'devapi\models\TaHasil';

    public function actions() {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update'], $actions['view'], $actions['index']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        //$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }
   
    public function actionProgram(){
    	$model = $this->modelClass;
    	$params = \Yii::$app->getRequest()->getQueryParams();
    	$subquery = $model::find()->select([
    		'Ta_Hasil.Tahun',
    		'Ta_Hasil.Kd_Urusan',
    		'Ta_Hasil.Kd_Bidang',
    		'Ta_Hasil.Kd_Prog',
            'Ta_Hasil.Kd_Unit',
            'Ta_Hasil.Kd_Sub'
    		//'Ta_Pagu_Program_Definitif.pagu',
    	])
    	->where(['Asal_Data' => 5])
        ->where(['!=','Kd_Prog', 0])
        ->where(['Kd_Tahapan' => 6]);
        if (isset($params['date']))
            $subquery->where(['>=', 'DateCreate', date('Y-m-d H:i:s'), $params['date']]);
        if (isset($params['dateEnd']))
            $subquery->where(['<=', 'DateCreate', date('Y-m-d H:i:s'), $params['dateEnd']]);
    	$subquery->groupBy('Ta_Hasil.Tahun, Ta_Hasil.Kd_Urusan, Ta_Hasil.Kd_Bidang, Ta_Hasil.Kd_Prog,  Ta_Hasil.Kd_Unit, Ta_Hasil.Kd_Sub');
        $query = $model::find()->select([
            'Ta_DASK.Tahun',
            'Ta_DASK.Kd_Urusan',
            'Ta_DASK.Kd_Bidang',
            'Ta_DASK.Kd_Unit',
            'Ta_DASK.Kd_Sub',
            'Ta_Program.Ket_Program',
            'm.Kd_Prog',
            'Ta_Pagu_Program_Definitif.pagu'
        ])->from('Ta_DASK')->innerJoin(['m' => $subquery], 'Ta_DASK.Tahun = m.Tahun AND Ta_DASK.Kd_Urusan = m.Kd_Urusan AND Ta_DASK.Kd_Bidang = m.Kd_Bidang AND Ta_DASK.Kd_Unit = m.Kd_Unit AND Ta_DASK.Kd_Sub = m.Kd_Sub')->innerJoin('Ta_Pagu_Program_Definitif', 'Ta_Pagu_Program_Definitif.Tahun = m.Tahun AND Ta_Pagu_Program_Definitif.Kd_Urusan = m.Kd_Urusan AND Ta_Pagu_Program_Definitif.Kd_Bidang = m.Kd_Bidang AND Ta_Pagu_Program_Definitif.Kd_Unit = m.Kd_Unit AND Ta_Pagu_Program_Definitif.Kd_Sub = m.Kd_Sub AND Ta_Pagu_Program_Definitif.Kd_Prog = m.Kd_Prog')
        ->innerJoin('Ta_Program', 'Ta_Program.Tahun = m.Tahun AND Ta_Program.Kd_Urusan = m.Kd_Urusan AND Ta_Program.Kd_Bidang = m.Kd_Bidang  AND Ta_Program.Kd_Prog = m.Kd_Prog');
        //->where(['not', ['Ta_Program.Ket_Prog' => null]])->where(['!=', 'Ta_Pagu_Program_Definitif.pagu', 0]);
    	return new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		        'pageSize' => 1000,
		    ],
		]);

    }

    public function actionKegiatan(){
    	$model = $this->modelClass;
    	$query = $model::find()->select([
    		'Ta_Hasil.Tahun',
    		'Ta_Hasil.Kd_Urusan',
    		'Ta_Hasil.Kd_Bidang',
    		'Ta_Hasil.Kd_Unit',
    		'Ta_Hasil.Kd_Sub',
    		'Ta_Hasil.Kd_Prog',
    		'Ta_Hasil.Kd_Keg',
    		'Ta_Hasil.Ket_Kegiatan',
    		new Expression('Ta_Pagu_Kegiatan_Definitif.pagu_keg as pagu'),
    	])->join('INNER JOIN', 'Ta_Pagu_Kegiatan_Definitif','Ta_Hasil.Tahun = Ta_Pagu_Kegiatan_Definitif.Tahun AND Ta_Hasil.Kd_Urusan = Ta_Pagu_Kegiatan_Definitif.Kd_Urusan AND Ta_Hasil.Kd_Bidang = Ta_Pagu_Kegiatan_Definitif.Kd_Bidang AND Ta_Hasil.Kd_Unit = Ta_Pagu_Kegiatan_Definitif.Kd_Unit AND Ta_Hasil.Kd_Sub = Ta_Pagu_Kegiatan_Definitif.Kd_Sub AND Ta_Hasil.Kd_Prog = Ta_Pagu_Kegiatan_Definitif.Kd_Prog AND Ta_Hasil.Kd_Keg = Ta_Pagu_Kegiatan_Definitif.Kd_Keg')
    	->join('INNER JOIN', 'Ta_DASK', 'Ta_Hasil.Tahun = Ta_DASK.Tahun AND Ta_Hasil.Kd_Urusan = Ta_DASK.Kd_Urusan AND Ta_Hasil.Kd_Bidang = Ta_DASK.Kd_Bidang AND Ta_Hasil.Kd_Unit = Ta_DASK.Kd_Unit AND Ta_Hasil.Kd_Sub = Ta_DASK.Kd_Sub')
		->where(['Asal_Data' => 2])
        ->where(['Kd_Tahapan' => 6]);
		if (isset($params['date']))
    		$query->where(['>=', 'DateCreate', date('Y-m-d H:i:s'), $params['date']]);
    	if (isset($params['dateEnd']))
    		$query->where(['<=', 'DateCreate', date('Y-m-d H:i:s'), $params['dateEnd']]);
    	return new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		        'pageSize' => 10000,
		    ],
		]);

    }

}
