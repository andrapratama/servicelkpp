<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use linslin\yii2\curl;
use \backend\models\TrxRekKoran;


class RekeningKoranController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        //'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new \yii\base\DynamicModel([
            'Tgl_Dari', 'Tgl_Sampai'
        ]);
        $model ->addRule(['Tgl_Dari','Tgl_Sampai'], 'safe');
        return $this->render('beranda', ['model' => $model]);
    }

    public function actionSinkron(){
        set_time_limit(300);
        $date1 = date('Y-m-d');
        $date2 = date('Ymd');
        $time1 = date('H:i:s');
        $time2 = date('His');
        $dateStart = $date1;
        $dateEnd = $date1;
        $model = new \yii\base\DynamicModel([
            'Tgl_Dari', 'Tgl_Sampai'
        ]);
        $model->addRule(['Tgl_Dari','Tgl_Sampai'], 'safe');
        if($model->load(\Yii::$app->request->post())){
            $dateStart = $model->Tgl_Dari;
            $dateEnd = $model->Tgl_Sampai;
        }
        $arr_lama = TrxRekKoran::find()->where([
            'between', 'TXDATE', $dateStart, $dateEnd
        ])->asArray()->all();
        $authKey = hash_hmac('sha1', '00008'.'100.30.40.2'.$date1.$time1, 'emogw');
        $curl = new curl\Curl();
        $params = json_encode([
                'authKey' => $authKey,
                'reqId' => '00008',
                'txDate' => $date2,
                'txHour' => $time2,
                'userGtw' => 'emogw',
                'channelId' => '33',
                'accNbr' => "10001010006230",
                'startDate' => $dateStart,
                'endDate' => $dateEnd
             ]);
        //echo $params;
        $response = $curl->setRequestBody($params)
             ->setHeaders([
                'Content-Type' => 'application/json',
                'Content-Length' => strlen($params)
             ])
             ->post('http://192.168.30.70:6565/Gateway/gateway/services/v2/postData');
        //echo "<pre>";
        $arr = json_decode($response, TRUE)['result'];
        //var_dump($response);exit;
        $arr_baru = array_udiff($arr, $arr_lama, [$this,'cek_perbedaan']);
        if (count($arr_baru) > 0){
            $model = new TrxRekKoran();
            for ($i = 0;$i < count($arr_baru);$i += 1000){
                $arr_temp = array_slice($arr_baru, $i, 1000);
                $arr_temp = $this->pembersihan($arr_temp);
                $db = TrxRekKoran::getDb();
                $db->createCommand()->batchInsert($model::tableName(), array_keys($arr_temp[0]), $arr_temp)->execute();
            }
        }
        echo "Data baru pada waktu ".$date1." ".$time1." adalah sebanyak ".count($arr_baru).".";
    }

    private function cek_perbedaan($a, $b){
        if ($a['TXID']===$b['TXID'] && strtotime($a['TXDTSTLMN'])==strtotime($b['TXDTSTLMN']))
        {
            return 0;
        }else if ($a['TXID']===$b['TXID']){
            return (strtotime($a['TXDTSTLMN']) > strtotime($b['TXDTSTLMN']))? 1 : -1;
        }else
            return ($a['TXID'] > $b['TXID'])? 1 : -1;
    }

    private function pembersihan($arr){
        $model = new TrxRekKoran();
        foreach ($arr as $value) {
            $arr_diff = array_diff(array_keys($value), $model->attributes());
            foreach ($arr_diff as $values) {
                unset($value[$values]);
            }
            $arr_temp[] = $value;
        }
        return $arr_temp;
    }

}
