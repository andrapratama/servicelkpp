<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;


class GenerateStsController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        //'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
    	echo '<pre>';
    	echo var_dump(\Yii::createObject(\yii\db\ActiveQuery::className(), [get_called_class()]));
    	exit; 
    	return $this->render('testin');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAmbilData(){
        $ZULTest = \yii\helpers\ArrayHelper::getColumn(\backend\models\BridgeJson::find()
            ->where(['id_db' => 'pokir'])->all(),'content');
        \backend\models\BridgeJson::deleteAll(['id_db' => 'pokir']);
        return \yii\helpers\Json::encode($ZULTest);
    }

    public function actionInsertData(){
        $data = \Yii::$app->request->post('data');
        //$datafix = 
        //var_dump(\yii\helpers\Json::decode($data[0]));exit;
        foreach ($data as $key => $value) {
            $datafix = \yii\helpers\Json::decode($value);
            $ZULTest = new \backend\models\RefLingkungan();
            $ZULTest->attributes = $datafix;
            $ZULTest->save(false);
        }
        
        return "Berhasil;";
       // print_r(\yii\helpers\Json::decode($ZULTest->content));
    }

    public function actionFormData(){
        //echo phpinfo();exit;
        $ZULForm = new \backend\models\RefSettingDb();
        $searchModel = new \backend\models\search\RefSettingDbSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if ($ZULForm->load(\Yii::$app->request->post())){
            $ZULForm->save(false);
            return $this->refresh();
        }
        return $this->render('form_input_dbase',[
            'model' => $ZULForm,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTestKoneksi(){
         $ZULForm = new \backend\models\RefSettingDb();
         if ($ZULForm->load(\Yii::$app->request->post())){
            $connection = new \yii\db\Connection([
            //'dsn' => 'mysql:host='.$ZULForm->IP.';port='.$ZULForm->port.
            //';dbname='.$ZULForm->db_name,
            'dsn' => 'sqlsrv:Server='.$ZULForm->ip.','.$ZULForm->port.';Database='.$ZULForm->db_name.';ConnectionPooling=0',

            'username' => $ZULForm->username,
            'password' => $ZULForm->password,
            ]);
            try{
                $connection->open();
                return "Berhasil";
            }catch(\yii\db\Exception $e){
                return var_dump($e->errorInfo);
            }
         }
    }

    public function actionBridge(){
        $ZULdatabase = \yii\helpers\ArrayHelper::map(\backend\models\RefSettingDb::find()->all(),
            'ID','nm_setting_db');
        return $this->render('form_bridge',[
            'data_konfig' => $ZULdatabase
        ]);
    }

    public function actionSts(){
        $ZULdatabase = \yii\helpers\ArrayHelper::map(\backend\models\RefSettingDb::find()->all(),
            'ID','nm_setting_db');
        return $this->render('form_sts',[
            'data_konfig' => $ZULdatabase
        ]);
    }

    public function actionEksporSts(){
        $ZULdatabase = \yii\helpers\ArrayHelper::map(\backend\models\RefSettingDb::find()->all(),
            'ID','nm_setting_db');
        return $this->render('form_ekspor_sts',[
            'data_konfig' => $ZULdatabase
        ]);
    }    

    public function actionEksporGabung(){
        $ZULdatabase = \yii\helpers\ArrayHelper::map(\backend\models\RefSettingDb::find()->all(),
            'ID','nm_setting_db');
        return $this->render('form_ekspor_gabung',[
            'data_konfig' => $ZULdatabase
        ]);
    }    

    public function actionNamaTable($id){
        $ZULForm = \backend\models\RefSettingDb::findOne(['ID' => $id]);
        $connection = new \yii\db\Connection([
                    'dsn' => 'sqlsrv:Server='.$ZULForm->ip.','.$ZULForm->port.';Database='.$ZULForm->db_name.';ConnectionPooling=0',
                    'username' => $ZULForm->username,
                    'password' => $ZULForm->password,
        ]);
        $tables = $connection->schema->getTableNames();
        return \yii\helpers\Html::renderSelectOptions([], $tables, $tagOptions);
        //var_dump($tables);
    }

    public function actionKolomTable($id, $name){
         $ZULForm = \backend\models\RefSettingDb::findOne(['id' => $id]);
         $connection = new \yii\db\Connection([
                   'dsn' => 'sqlsrv:Server='.$ZULForm->ip.','.$ZULForm->port.';Database='.$ZULForm->db_name.';ConnectionPooling=0',
                    'username' => $ZULForm->username,
                    'password' => $ZULForm->password,
        ]);
        $tables = $connection->schema->getTableSchema($name)->columns;
       //print_r($tables);exit;
        $Kolom = \yii\helpers\ArrayHelper::getColumn($tables, 'name');
        return '<label>Kolom Tabel</label>'. 
        \yii\helpers\Html::radioList('nama-kolom', '', $Kolom, [ 'id' => 'nama-kolom']);
    }

    public function actionTestingKoneksi(){
        set_time_limit(600);
        $data = \Yii::$app->request->post();
        //print_r($data['konfig']);exit;
        $ZULForm = \backend\models\RefSettingDb::findOne(['ID' => $data['konfig']]);
        if (isset($data['konfig1']))
            $ZULForm1 = \backend\models\RefSettingDb::findOne(['ID' => $data['konfig1']]);
        $connection1 = new \yii\db\Connection([
            'dsn' => 'sqlsrv:Server='.$ZULForm->ip.','.$ZULForm->port.';Database='.$ZULForm->db_name.';ConnectionPooling=0',
            'username' => $ZULForm->username,
            'password' => $ZULForm->password,
        ]);
        if (isset($data['konfig1']))
        $connection2 = new \yii\db\Connection([
            'dsn' => 'sqlsrv:Server='.$ZULForm1->ip.','.$ZULForm1->port.';Database='.$ZULForm1->db_name.';ConnectionPooling=0',
            'username' => $ZULForm1->username,
            'password' => $ZULForm1->password,
        ]);
        try{
            $connection1->open();
            $output = preg_split( "/(:|=|;|,)/", $connection1->dsn );
            echo "Test Koneksi IP: ".$output[2]." Berhasil&#13;&#10;";
            echo "- Port: ".$output[3]." Berhasil&#13;&#10;";
            echo "- Database: ".$output[5]." Berhasil&#13;&#10;&#13;&#10;";
            
        }catch(\yii\db\Exception $e){
            return var_dump($e->errorInfo);
        }
        if (isset($data['konfig1']))
            try{
                $connection2->open();
                 $output = preg_split( "/(:|=|;|,)/", $connection2->dsn );
                echo "Test Koneksi IP: ".$output[2]." Berhasil&#13;&#10;";
                echo "- Port: ".$output[3]." Berhasil&#13;&#10;";
                echo "- Database: ".$output[5]." Berhasil&#13;&#10;&#13;&#10;";
            }catch(\yii\db\Exception $e){
                return var_dump($e->errorInfo);
            }
    }
    
    public function actionTaSts(){
        $ZULdatabase = \yii\helpers\ArrayHelper::map(\backend\models\RefSettingDb::find()->all(),
            'ID','nm_setting_db');
        return $this->render('form_sinkron_sts',[
            'data_konfig' => $ZULdatabase
        ]);
    }

    public function actionSinkronisasiTaSts(){
	set_time_limit(600);
        $data = \Yii::$app->request->post();
        //print_r($data['konfig']);exit;
        $ZULForm = \backend\models\RefSettingDb::findOne(['ID' => $data['konfig']]);
        $ZULForm1 = \backend\models\RefSettingDb::findOne(['ID' => $data['konfig1']]);
        $connection1 = new \yii\db\Connection([
            'dsn' => sprintf($ZULForm->koneksi, $ZULForm->ip, $ZULForm->port, $ZULForm->db_name),
            'username' => $ZULForm->username,
            'password' => $ZULForm->password,
         ]);
        $connection2 = new \yii\db\Connection([
            'dsn' => sprintf($ZULForm1->koneksi, $ZULForm1->ip, $ZULForm1->port, $ZULForm1->db_name),
            'username' => $ZULForm1->username,
            'password' => $ZULForm1->password,
         ]);
         $count = $connection1->createCommand("EXEC dbo.SP_INSERT_STSHistory_TaSTS")
                   ->queryOne();
         $count1 = $connection2->createCommand("EXEC dbo.SP_INSERT_STSHistory_TaSTS")
                   ->queryOne();
         echo date("Y/m/d h:i:s").": Data Rekening baru ".$count["Hitung"]. " data.&#13;&#10;";
         echo date("Y/m/d h:i:s").": Data STS baru ".$count1["Hitung"]. " data.";
         
    }

    public function actionGenerateSts(){
        set_time_limit(600);
        $data = \Yii::$app->request->post();
        $ZULForm = \backend\models\RefSettingDb::findOne(['ID' => $data['konfig']]);
        //print_r(sprintf($ZULForm->koneksi, $ZULForm->ip, $ZULForm->port, $ZULForm->db_name));exit;
        $connection1 = new \yii\db\Connection([
            'dsn' => sprintf($ZULForm->koneksi, $ZULForm->ip, $ZULForm->port, $ZULForm->db_name),
            'username' => $ZULForm->username,
            'password' => $ZULForm->password,
         ]);
        
        $names = $connection1->schema->getTableNames();
        $data1 = $connection1->createCommand('select count(*) from '.$names[$data['nama_table']].' where No_STS is null')
                ->queryScalar();
	$count = 0;
        $count = $connection1->createCommand("EXEC dbo.SP_Generate_STS :paramName1, :paramName2, :paramName3") 
                      ->bindValue(':paramName1' , $names[$data['nama_table']] )
                      ->bindValue(':paramName2', ' ')
                      ->bindValue(':paramName3', ' ')
                      ->queryOne();
        echo date("Y/m/d h:i:s").": Data STS baru ".$count["Hitung"]. " data.";
           
    }

   

    public function actionImportGabung(){
        set_time_limit(600);
        $data = \Yii::$app->request->post();
        //print_r($data['konfig']);exit;
        $ZULForm = \backend\models\RefSettingDb::findOne(['ID' => $data['konfig']]);
        $ZULForm1 = \backend\models\RefSettingDb::findOne(['ID' => $data['konfig1']]);
        $connection1 = new \yii\db\Connection([
            'dsn' => 'sqlsrv:Server='.$ZULForm->ip.','.$ZULForm->port.';Database='.$ZULForm->db_name.';ConnectionPooling=0',
            'username' => $ZULForm->username,
            'password' => $ZULForm->password,
        ]);$connection2 = new \yii\db\Connection([
            'dsn' => 'sqlsrv:Server='.$ZULForm1->ip.','.$ZULForm1->port.';Database='.$ZULForm1->db_name.';ConnectionPooling=0',
            'username' => $ZULForm1->username,
            'password' => $ZULForm1->password,
        ]);
        $names = $connection1->schema->getTableNames();
        /*$data1 = $connection1->createCommand("select nosts from "."Transaction2 where nosts is not null and valid = 'yes' and Date between dateadd(day, datediff(day, 1, GETDATE()),0) and GETDATE() order by nosts asc")
                ->queryColumn();//print_r($data1);*/
        $names2 = $connection2->schema->getTableNames();
        /*$data2 = $connection2->createCommand('select nosts from '.$names2[$data['nama_table1']].' where nosts is not null and Date between dateadd(day, datediff(day, 2, GETDATE()),0) and GETDATE() order by nosts asc')
                ->queryColumn();//print_r($data2);*/
        //print_r($names2[$data['nama_table1']]);exit;
        $tables = $connection2->schema->getTableSchema($names2[$data['nama_table1']])->columns;
        $Kolom = \yii\helpers\ArrayHelper::getColumn($tables, 'name');
        $Kolom = array_diff($Kolom, ["ID_Sinkron"]);
        //print_r($Kolom);exit;
        /*$nosts = array_diff_key($data1, $data2);//print_r(count($nosts));exit;*/
        $i = 0;$data3 = 0;
        $data4 = $connection1->createCommand("select count(*) from Transaction2 where Transaction2.valid='yes' and Date between '2017/01/01' and '2017/01/31' ")->queryScalar();
       
            //$kolom_bantu = array_slice($nosts, $i, 1000);

            $rows = (new \yii\db\Query())->select("dbo.SKPD.Name,  
                (dbo.Account2.R1 + '.' + dbo.Account2.R2 + '.' +
                dbo.Account2.R3 + '.' + dbo.Account2.R4 + '.' +
                dbo.Account2.R5 + '.' + dbo.Account2.R6 + '.' + 
                dbo.Account2.R7 + '.' + dbo.Account2.R8 + '.' +
                dbo.Account2.R9 + '.' + dbo.Account2.R10 + ' ' +
                dbo.Account2.Name),
                dbo.Transaction2.Date,  CONVERT(bigint,SUM(dbo.Transaction2D.Credit)) AS Credit, 
                dbo.Transaction2.nosts,dbo.Transaction2.keterangan, dbo.Transaction2.ID2 , dbo.Transaction2.no_bukti")
                ->from('dbo.Transaction2')
                ->innerJoin('dbo.rek_penerimaan','dbo.Transaction2.ID = dbo.rek_penerimaan.id_transactions2','')
                ->innerJoin('dbo.SKPD', 'dbo.Transaction2.ID_SKPD = dbo.SKPD.ID', '')
                ->innerJoin('dbo.Account2', 'dbo.Transaction2.Account2ID = dbo.Account2.ID', '')
                ->leftJoin('dbo.Transaction2D','dbo.Transaction2.ID = dbo.Transaction2D.Transaction2ID','')
                ->where("Transaction2.valid='yes' and Date between '2017/01/01' and '2017/01/31'")
                //->andWhere(["Transaction2.nosts" => $kolom_bantu])
                ->groupBy(['dbo.Transaction2.ID', 'dbo.Transaction2.ID2', 'dbo.Transaction2.TransactionID', 'dbo.Transaction2.Account2ID', 'dbo.Transaction2.UPTID', 'dbo.Transaction2.Date', 
                'dbo.Transaction2.Adjustment', 'dbo.Transaction2.Mutation', 'dbo.Transaction2.Note', 'dbo.Transaction2.remote', 'dbo.Transaction2.id_kaspembantu', 
                'dbo.Transaction2.nosts', 'dbo.Transaction2.id_keterangan', 'dbo.Transaction2.ID_SKPD', 'dbo.Transaction2.id_client', 'dbo.Transaction2.keterangan', 
                'dbo.Transaction2.origindata', 'dbo.Transaction2.valid', 'dbo.Transaction2.no_bukti', 'dbo.rek_penerimaan.TGL_TX', 'dbo.rek_penerimaan.id_transactions2','dbo.SKPD.Name', 'dbo.Account2.R1', 'dbo.Account2.R2','dbo.Account2.R3', 'dbo.Account2.R4', 'dbo.Account2.R5','dbo.Account2.R6'
                ,'dbo.Account2.R7','dbo.Account2.R8','dbo.Account2.R9','dbo.Account2.R10', 'dbo.Account2.Name'])
                ->orderBy("dbo.Transaction2.Date asc");
            //echo "apalah";exit;
            //print_r($syntax->createCommand()->queryAll());exit;
            foreach($rows->batch(1000, $connection1) as $row){
            $connection2->createCommand()->batchInsert($names2[$data['nama_table1']], $Kolom, $row)->execute();
            $data3 += count($rows);

            $i += 1000;
        }
        echo date("Y/m/d h:i:s").": Data baru terunduh sebanyak ".$data3. " data.";

    }

    protected function arr_to_str($arr){
        return  implode(', ', array_map(function($entry){
                    return '('.$entry["id"].','.$entry["nosts"].')'; 
                }, $arr));
    }

}
