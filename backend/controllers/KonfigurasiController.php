<?php

namespace backend\controllers;

use Yii;
use backend\models\RefSettingDb;
use backend\models\RefSettingDbSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * KonfigurasiController implements the CRUD actions for RefSettingDb model.
 */
class KonfigurasiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        //'actions' => ['logout', 'index','create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefSettingDb models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RefSettingDbSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefSettingDb model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RefSettingDb model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefSettingDb();
       // print_r(Yii::$app->request->post());exit;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            //print_r($model->getErrors());exit;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RefSettingDb model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefSettingDb model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RefSettingDb model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefSettingDb the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefSettingDb::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionTestKoneksi(){
         $ZULForm = new \backend\models\RefSettingDb();
         
         if ($ZULForm->load(\Yii::$app->request->post())){
            // print_r($ZULForm);exit;
           // print_r($ZULForm);
            $ip = $ZULForm->ip;
            $port = $ZULForm->port;
            $db_name = $ZULForm->db_name;
           // echo  sprintf($ZULForm->koneksi, $ip, $port, $db_name);exit;
            $connection = new \yii\db\Connection([
            //'dsn' => 'mysql:host='.$ZULForm->IP.';port='.$ZULForm->port.
            //';dbname='.$ZULForm->db_name,
            //'dsn' => 'sqlsrv:Server='.$ZULForm->ip.','.$ZULForm->port.';Database='.$ZULForm->db_name.';ConnectionPooling=0',
            //'dsn' => 'oci:dbname='.$ZULForm->ip.':'.$ZULForm->port.'/'.$ZULForm->db_name.';charset=UTF8',
            'dsn' => sprintf($ZULForm->koneksi, $ip, $port, $db_name),
            'username' => $ZULForm->username,
            'password' => $ZULForm->password,
            ]);
            try{
                $connection->open();
                echo "Berhasil";
            }catch(\yii\db\Exception $e){
                echo 'oci:dbname='.$ZULForm->ip.':'.$ZULForm->port.'/'.$ZULForm->db_name.';charset=UTF8'.var_dump($e);
            }
         }
    }
}
