<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use linslin\yii2\curl;
use \backend\models\RefVerifikasiAsn;
use \backend\models\RefApi;


class PaketKerasController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        //'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new \yii\base\DynamicModel([
            'Tgl_Dari', 'Tgl_Sampai'
        ]);
        $model ->addRule(['Tgl_Dari','Tgl_Sampai'], 'safe');
        return $this->render('beranda', ['model' => $model]);
    }

    public function actionSinkron(){
        set_time_limit(600);
        $modelApi = RefApi::find()->where([
            'id_name' => 'pkeras',
            'tipe' => 'data'
        ])->one();
        $curl = new curl\Curl();
        $response = $curl
             ->setHeaders([
                'Authorization' => 'Bearer '.Yii::$app->key->tokenPaketKeras
             ])
             ->post($modelApi->link);
        $total = json_decode($response, true)['total_data'];
        $totalIterasi = ceil($total / 1000);
        $totalBaru = 0;
        for ($i = 0; $i < $totalIterasi; $i++){
            $curl = new curl\Curl();
            $response = $curl
                ->setHeaders([
                    'Authorization' => 'Bearer '.Yii::$app->key->tokenPaketKeras
                ])
                ->post($modelApi->link.'?page='.$i);
            //echo $modelApi->link.'?page='.$i;
            $Asn = json_decode($response, true)['data'];
            $allAsn = RefVerifikasiAsn::find()->asArray()->all();
            $dataBaru = array_udiff($Asn, $allAsn, [$this,'cek_perbedaan_baru']);
            if (count($dataBaru) != 0){
                $db = RefVerifikasiAsn::getDb();
                $db->createCommand()->batchInsert(RefVerifikasiAsn::tableName(), array_keys($dataBaru[0]), $dataBaru)->execute();
            }
            $totalBaru += count($dataBaru);
        }
        
        /*foreach($Asn as $modelAsn){
            $modAsn = RefVerifikasiAsn::find()->where(['nip' => $modelAsn->nip])->one();
            if ($modAsn == null || $modAsn->onupdated != $modelAsn->onupdated){
                if ($modAsn == null){
                    $modAsn = new RefVerifikasiAsn();
                    $modAsn->nip = $modelAsn->nip;
                }
                $modAsn->nama = $modelAsn->nama;
                $modAsn->pangkat = $modelAsn->pangkat;
                $modAsn->jenis_jabatan = $modelAsn->jenis_jabatan;
                $modAsn->nama_jabatan = $modelAsn->nama_jabatan;
                $modAsn->tmt_jabatan = $modelAsn->tmt_jabatan;
                $modAsn->foto = $modelAsn->foto;
                $modAsn->alamat = $modelAsn->alamat;
                $modAsn->email = $modelAsn->email;
                $modAsn->status_kepegawaian = $modelAsn->status_kepegawaian;
                $modAsn->oncreated = $modelAsn->oncreated;
                $modAsn->onupdated = $modelAsn->onupdated;
                $modAsn->golongan = $modelAsn->golongan;
                $modAsn->save();
            }
        }*/
       echo "Data baru/update pada waktu ".date('Y-m-d H:i:s')." adalah sebanyak ".count($dataBaru).".";
    }

    private function cek_perbedaan_baru($a, $b){
        if ($a['nip'] == $b['nip']) {
            return 0;
        } elseif ($a['nip'] > $b['nip']) {
            return 1;
        } else {
            return -1;
        }
    }

    private function pembersihan($arr){
        $model = new TrxRekKoran();
        foreach ($arr as $value) {
            $arr_diff = array_diff(array_keys($value), $model->attributes());
            foreach ($arr_diff as $values) {
                unset($value[$values]);
            }
            $arr_temp[] = $value;
        }
        return $arr_temp;
    }

}
