<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RefSettingDb */
/* @var $form yii\widgets\ActiveForm */

$js = '$("#tes_koneksi").click(function(){
            $(this).prop("disabled", true);
            $("#status").html("Tunggu");
            $.ajax({
                url: "index.php?r=konfigurasi/test-koneksi",
                type: "POST",
                data: $("#form_input_dbase").serialize(),
                success: function(data){
                    alert(data);
                    $("#status").html(data);
                },
                error: function(data){
                    //$("#status").html("Gagal");
                    alert(data.responseText);
                }
            });
            $(this).prop("disabled", false);
    });';
    $this->registerJs($js);
?>

<div class="ref-setting-db-form">

    <?php $form = ActiveForm::begin(['id' => 'form_input_dbase']); ?>

    <?= $form->field($model, 'nm_setting_db')->textInput(['id' => 'name'])->label("Nama Konfigurasi") ?>

    <?= $form->field($model, 'ip')->textInput(['id' => 'ip'])->label("IP") ?>

    <?= $form->field($model, 'port')->textInput(['id' => 'port'])->label("Port") ?>

    <?= $form->field($model, 'username')->textInput(['id' => 'username'])->label("Username") ?>

    <?= $form->field($model, 'password')->passwordInput(['id' => 'password'])->label("Password") ?>

    <?= $form->field($model, 'db_name')->textInput(['id' => 'db_name'])->label("Nama Database") ?>

     <?= $form->field($model, 'skema')->textInput()->label("Skema") ?>

     <?= $form->field($model, 'koneksi')->textInput()->label("String Koneksi") ?>

    <?= $form->field($model, 'waktu_interval')->textInput()->label("Waktu Interval") ?>


    <div class="form-group">
        <?= Html::button('Uji Koneksi',['class' => 'btn btn-danger', 'id' => 'tes_koneksi',
            'data-toggle' => 'modal',
            'data-target' => '#modal_nihil'
        ]) ?>
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
