<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

///use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
//use \yii\helpers\Html;


$js1 = $this->registerJsFile(
    '@web/js/rekeningkoran.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Rekening Koran</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="#" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Beranda</a></li>
                    <li class="active">Rekening Koran</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <!-- ============================================================== -->
        <!-- Different data widgets -->
        <!-- ============================================================== -->
        <div class ="white-box">
            <div class="row">
                <div class="col-md-12">
                    <b>Log Sinkron Rekening Koran</b>
                </div>
                <br><br>
                <div class="col-md-12 form-group">
                    <label class="checkbox-inline "><b>Metode : </b></label>
                    <label class="checkbox-inline"><input type="radio" value="1" name="metode" class="metode"> Sinkronisasi</label>
                    <label class="checkbox-inline"><input type="radio" value="2" name="metode" class="metode"> Sekali</label>
                </div>
                <div class="col-md-12 form-group">
                    <?php $form = ActiveForm::begin(['id' => 'form']); ?>
 
                        <?= $form->field($model, 'Tgl_Dari')->widget(DatePicker::className(), [
                            //'language' => 'ru',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => [
                                'class' => 'form form-control',
                                'disabled' => true
                            ]
                        ])->label('Tanggal Mulai') ?>
                        <?= $form->field($model, 'Tgl_Sampai')->widget(DatePicker::className(), [
                            //'language' => 'ru',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => [
                                'class' => 'form form-control',
                                'disabled' => true
                            ]

                        ])->label('Tanggal Selesai') ?>
                    <?php  ActiveForm::end(); ?>
                </div>
                <br><br>
                <div class="col-md-12 form-group">
                    <?= Html::Button('Mulai', ['class' => 'btn btn-primary btn-block',  'id' => 'btn-tekan']); ?>
                </div>
                <div class="col-md-12 form-group">
                    <br><br>
                    <?= Html::textarea('keterangan', '', ['class' => 'form form-control', 'rows' => '20', 'id' => 'keterangan']); ?>
                    <br><br>
                    
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- ============================================================== -->
        <!-- start right sidebar -->
        <!-- ============================================================== -->
        <div class="right-sidebar">
            <div class="slimscrollright">
                <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                <div class="r-panel-body">
                    <ul id="themecolors" class="m-t-20">
                        <li><b>With Light sidebar</b></li>
                        <li><a href="javascript:void(0)" theme="default" class="default-theme working">1</a></li>
                        <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                        <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                        <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                        <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                        <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                        <li><b>With Dark sidebar</b></li>
                        <br/>
                        <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                        <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                        <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>
                        <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                        <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                        <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                    </ul>
                    <ul class="m-t-20 chatonline">
                        <li><b>Chat option</b></li>
                        <li>
                            <a href="javascript:void(0)"><img src="plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- /.container-fluid -->