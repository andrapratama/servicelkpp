var id1;
var start = 0;
var line = 0;
$(document).on('click', '#btn-tekan', function(){
    if (start == 0){
        $("#keterangan").html("");
        line = 0;
        alert($('[name=metode]:checked').val());
        if ($('[name=metode]:checked').val() == '1'){
            $.ajax({
                url: "index.php?r=paket-keras/sinkron",
                success: function (data) {
                    id1 = setInterval(get_data, 120000);
                    $("#keterangan").append(data + "&#13;&#10;");
                    $('#btn-tekan').html('Berhenti');
                    $('#btn-tekan').attr('class', 'btn btn-danger btn-block');
                    start = 1;
                },
                error: function (xhr, stat, err) {
                    $("#keterangan").append(xhr.responseText + "&#13;&#10;");
                }

            });
        }else{
             $('#btn-tekan').html('Sedang proses ...');
             $('#btn-tekan').attr('disabled', true);
             $.ajax({
                url: "index.php?r=paket-keras/sinkron",
                data: $('#form').serialize(),
                type: "POST",
                success: function (data) {
                    $("#keterangan").append(data + "&#13;&#10;");
                    $('#btn-tekan').html('Mulai');
                    $('#btn-tekan').attr('disabled', false);
                },
                error: function (xhr, stat, err) {
                    $("#keterangan").append(xhr.responseText + "&#13;&#10;");
                    $('#btn-tekan').html('Mulai');
                    $('#btn-tekan').attr('disabled', false);
                }

            });
        }
    }else{
        clearInterval(id1);
        $("#keterangan").append("Service Berhenti!");
        $('#btn-tekan').html('Mulai');
        $('#btn-tekan').attr('class', 'btn btn-primary btn-block');
        start = 0;
    }
})
function get_data() {
    line += 1;
    if (line == 101){
        $("#keterangan").html("");
        line = 0;
    }
    $.ajax({
        url: "index.php?r=paket-keras/sinkron",
        success: function (data) {
            $("#keterangan").append(data + "&#13;&#10;");
        },
        error: function (xhr, stat, err) {
            $("#keterangan").append(xhr.responseText + "&#13;&#10;");
        }

    });

}


$(document).on('change', '.metode', function(){
    if ($(this).val() == '1'){
        $('[name="DynamicModel[Tgl_Dari]"]').attr('disabled', true);
        $('[name="DynamicModel[Tgl_Sampai]"]').attr('disabled', true);
    }else{
        $('[name="DynamicModel[Tgl_Dari]"]').attr('disabled', false);
        $('[name="DynamicModel[Tgl_Sampai]"]').attr('disabled', false);
    }
})




