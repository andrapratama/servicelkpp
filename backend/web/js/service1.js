$("#tes_koneksi").click(function () {
    $(this).prop("disabled", true);
    $("#status").html("Tunggu");
    $.ajax({
        url: "index.php?r=site/test-koneksi",
        type: "POST",
        data: $("#form_input_dbase").serialize(),
        success: function (data) {
            $("#status").html(data);
        },
        failure: function (data) {
            $("#status").html("Gagal");
        }
    });
    $(this).prop("disabled", false);
});