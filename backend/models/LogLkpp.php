<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Log_Lkpp".
 *
 * @property string $Jenis
 * @property string $waktuAkses
 * @property int $Tahun
 * @property int $date
 * @property int $dateEnd
 */
class LogLkpp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Log_Lkpp';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Jenis'], 'string'],
            [['waktuAkses'], 'safe'],
            [['Tahun', 'date', 'dateEnd'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Jenis' => 'Jenis',
            'waktuAkses' => 'Waktu Akses',
            'Tahun' => 'Tahun',
            'date' => 'Date',
            'dateEnd' => 'Date End',
        ];
    }
}
