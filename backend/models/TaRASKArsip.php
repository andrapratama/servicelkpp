<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ta_RASK_Arsip".
 *
 * @property int $Tahun
 * @property int $Kd_Perubahan
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property int $Kd_Prog
 * @property int $ID_Prog
 * @property int $Kd_Keg
 * @property int $Kd_Rek_1
 * @property int $Kd_Rek_2
 * @property int $Kd_Rek_3
 * @property int $Kd_Rek_4
 * @property int $Kd_Rek_5
 * @property int $No_Rinc
 * @property int $No_ID
 * @property string $Keterangan_Rinc
 * @property string $Sat_1
 * @property string $Nilai_1
 * @property string $Sat_2
 * @property string $Nilai_2
 * @property string $Sat_3
 * @property string $Nilai_3
 * @property string $Satuan123
 * @property string $Jml_Satuan
 * @property string $Nilai_Rp
 * @property string $Total
 * @property string $Keterangan
 * @property int $Kd_Ap_Pub
 * @property int $Kd_Sumber
 * @property string $DateCreate
 *
 * @property RefSumberDana $tahun
 * @property TaKegiatan $kdKeg
 * @property RefRek5 $kdRek5
 */
class TaRASKArsip extends \yii\db\ActiveRecord
{

    //adding aliases column
    public $pagu;
    public $id_seq_prog;
    public $id_seq_keg;
    public $id_seq_belanja;
    public $id_seq_rinc_belanja;
    public $id_seq_rinc_belanja_sub;
    public $Kd_Unit_1;
    public $Kd_Sub_1;
    public $Ket_Program;
    public $Ket_Kegiatan;
    public $Nm_Rek_5;
    public $CREATED_TIME;
    public $LASTUPDATED_TIME;
    public $BTLP;
    public $BTL;
    public $BLP;
    public $BLBJ;
    public $BLM;
    public $id_unit;
    public $KLDI;
    public $Kd_Urusan1;
    public $Kd_Bidang1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RASK_Arsip';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Perubahan', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'No_Rinc', 'No_ID', 'Keterangan_Rinc'], 'required'],
            [['Tahun', 'Kd_Perubahan', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'No_Rinc', 'No_ID', 'Kd_Ap_Pub', 'Kd_Sumber'], 'integer'],
            [['Keterangan_Rinc', 'Sat_1', 'Sat_2', 'Sat_3', 'Satuan123', 'Keterangan'], 'string'],
            [['Nilai_1', 'Nilai_2', 'Nilai_3', 'Jml_Satuan', 'Nilai_Rp', 'Total'], 'number'],
            [['DateCreate'], 'safe'],
            [['ID_Prog', 'Kd_Bidang', 'Kd_Keg', 'Kd_Perubahan', 'Kd_Prog', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'No_ID', 'No_Rinc', 'Tahun'], 'unique', 'targetAttribute' => ['ID_Prog', 'Kd_Bidang', 'Kd_Keg', 'Kd_Perubahan', 'Kd_Prog', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'No_ID', 'No_Rinc', 'Tahun']],
            [['Tahun'], 'exist', 'skipOnError' => true, 'targetClass' => RefSumberDana::className(), 'targetAttribute' => ['Tahun' => 'Tahun']],
            [['Kd_Keg'], 'exist', 'skipOnError' => true, 'targetClass' => TaKegiatan::className(), 'targetAttribute' => ['Kd_Keg' => 'Kd_Keg']],
            [['Kd_Rek_5'], 'exist', 'skipOnError' => true, 'targetClass' => RefRek5::className(), 'targetAttribute' => ['Kd_Rek_5' => 'Kd_Rek_5']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Perubahan' => 'Kd  Perubahan',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'Kd_Prog' => 'Kd  Prog',
            'ID_Prog' => 'Id  Prog',
            'Kd_Keg' => 'Kd  Keg',
            'Kd_Rek_1' => 'Kd  Rek 1',
            'Kd_Rek_2' => 'Kd  Rek 2',
            'Kd_Rek_3' => 'Kd  Rek 3',
            'Kd_Rek_4' => 'Kd  Rek 4',
            'Kd_Rek_5' => 'Kd  Rek 5',
            'No_Rinc' => 'No  Rinc',
            'No_ID' => 'No  ID',
            'Keterangan_Rinc' => 'Keterangan  Rinc',
            'Sat_1' => 'Sat 1',
            'Nilai_1' => 'Nilai 1',
            'Sat_2' => 'Sat 2',
            'Nilai_2' => 'Nilai 2',
            'Sat_3' => 'Sat 3',
            'Nilai_3' => 'Nilai 3',
            'Satuan123' => 'Satuan123',
            'Jml_Satuan' => 'Jml  Satuan',
            'Nilai_Rp' => 'Nilai  Rp',
            'Total' => 'Total',
            'Keterangan' => 'Keterangan',
            'Kd_Ap_Pub' => 'Kd  Ap  Pub',
            'Kd_Sumber' => 'Kd  Sumber',
            'DateCreate' => 'Date Create',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTahun()
    {
        return $this->hasOne(RefSumberDana::className(), ['Tahun' => 'Tahun']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKdKeg()
    {
        return $this->hasOne(TaKegiatan::className(), ['Kd_Keg' => 'Kd_Keg']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKdRek5()
    {
        return $this->hasOne(RefRek5::className(), ['Kd_Rek_5' => 'Kd_Rek_5']);
    }

    public function fields(){
        switch (Yii::$app->controller->action->uniqueId) {
            case 'lkpp/program':
                return $this->lkpp_program();
                break;
            
            case 'lkpp/kegiatan':
                return $this->lkpp_kegiatan();
                break;
            case 'lkpp/belanja':
                return $this->lkpp_belanja();
                break;
            case 'lkpp/rincian':
                return $this->lkpp_rincian();
                break;
            case 'lkpp/anggaran':
                return $this->lkpp_anggaran();
                break;
            default:
                return parent::fields();
                break;
        }
    }

    public function lkpp_program(){
        return [
            'ID_PROGRAM' => function(){
                return $this->id_seq_prog;
            },
            'ID_SATKER'  => function(){
                return $this->Kd_Urusan . '.'.str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Sub, 2, '0', STR_PAD_LEFT);
            },
            'NAMA_PROGRAM' => function(){
                return $this->Ket_Program;
            },
            'KODE_PROGRAM' => function(){
                return $this->Kd_Urusan1 . '.'. str_pad($this->Kd_Bidang1, 2, '0', STR_PAD_LEFT) . '.'. $this->Kd_Urusan . '.'. str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'. str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Prog, 2, '0', STR_PAD_LEFT);
            },
            'PAGU'  => function(){
                return number_format($this->pagu,0,'','');
            },
            'IS_DELETED' => function(){
                return false;
            },
            'CREATE_TIME' => function(){
                //var_dump($this->CREATED_TIME);exit;
                return strtotime($this->CREATED_TIME);
            },
            'LASTUPDATE_TIME' => function(){
                return strtotime($this->LASTUPDATED_TIME);
            }
        ];
    }

    public function lkpp_kegiatan(){
        return [
            'ID_PROGRAM' => function(){
                return $this->id_seq_prog;
            },
            'ID_KEGIATAN' => function(){
                return $this->id_seq_keg;
            },
            'ID_SATKER'  => function(){
                return $this->Kd_Urusan . '.'.str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Sub, 2, '0', STR_PAD_LEFT);
            },
            'NAMA_KEGIATAN' => function(){
                return $this->Ket_Kegiatan;
            },
            'KODE_KEGIATAN' => function(){
                $kode = strlen($this->Kd_Keg) == 3 ? $this->Kd_Keg : str_pad($this->Kd_Keg, 2, '0', STR_PAD_LEFT);
                return $this->Kd_Urusan1 . '.'. str_pad($this->Kd_Bidang1, 2, '0', STR_PAD_LEFT) . '.'. $this->Kd_Urusan . '.'. str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'. str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Prog, 2, '0', STR_PAD_LEFT).'.'.$kode;
            },
            'PAGU'  => function(){
                return number_format($this->pagu,0,'','');
            },
            'IS_DELETED' => function(){
                return false;
            },
            'CREATE_TIME' => function(){
                //var_dump($this->CREATED_TIME);exit;
                return strtotime($this->CREATED_TIME);
            },
            'LASTUPDATE_TIME' => function(){
                return strtotime($this->LASTUPDATED_TIME);
            }
        ];
    }

    public function lkpp_belanja(){
        return [
            'ID_PROGRAM' => function(){
                return $this->id_seq_prog;
            },
            'ID_KEGIATAN' => function(){
                return $this->id_seq_keg;
            },
            'ID_OBJEK_AKUN' => function(){
                return $this->id_seq_belanja;
            },
            'ID_SATKER'  => function(){
                return $this->Kd_Urusan . '.'.str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Sub, 2, '0', STR_PAD_LEFT);
            },
            'NAMA_OBJEK_AKUN' => function(){
                return $this->Nm_Rek_5;
            },
            'KODE_OBJEK_AKUN' => function(){
                $kode = strlen($this->Kd_Keg) == 3 ? $this->Kd_Keg : str_pad($this->Kd_Keg, 2, '0', STR_PAD_LEFT);
                return $this->Kd_Urusan1 . '.'. str_pad($this->Kd_Bidang1, 2, '0', STR_PAD_LEFT) . '.'. $this->Kd_Urusan . '.'. str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'. str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Prog, 2, '0', STR_PAD_LEFT).'.'.$kode . '.'.$this->Kd_Rek_1.'.'.$this->Kd_Rek_2.'.'.$this->Kd_Rek_3.'.'.str_pad($this->Kd_Rek_4, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Rek_5, 2, '0', STR_PAD_LEFT);
            },
            'PAGU'  => function(){
                return number_format($this->pagu,0,'','');
            },
            'IS_DELETED' => function(){
                return false;
            },
            'CREATE_TIME' => function(){
                //var_dump($this->CREATED_TIME);exit;
                return strtotime($this->CREATED_TIME);
            },
            'LASTUPDATE_TIME' => function(){
                return strtotime($this->LASTUPDATED_TIME);
            }
        ];
    }

    public function lkpp_rincian(){
        return [
            'ID_PROGRAM' => function(){
                return $this->id_seq_prog;
            },
            'ID_KEGIATAN' => function(){
                return $this->id_seq_keg;
            },
            'ID_OBJEK_AKUN' => function(){
                return $this->id_seq_belanja;
            },
            'ID_RINCI_OBJEK_AKUN' => function(){
                return $this->id_seq_rinc_belanja;
            },
            'ID_SATKER'  => function(){
                return $this->Kd_Urusan . '.'.str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Sub, 2, '0', STR_PAD_LEFT);
            },
            'NAMA_RINCI_OBJEK_AKUN' => function(){
                return $this->Keterangan_Rinc;
            },
            'KODE_RINCI_OBJEK_AKUN' => function(){
                $kode = strlen($this->Kd_Keg) == 3 ? $this->Kd_Keg : str_pad($this->Kd_Keg, 2, '0', STR_PAD_LEFT);
                return $this->Kd_Urusan1 . '.'. str_pad($this->Kd_Bidang1, 2, '0', STR_PAD_LEFT) . '.'. $this->Kd_Urusan . '.'. str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'. str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Prog, 2, '0', STR_PAD_LEFT).'.'.$kode . '.'.$this->Kd_Rek_1.'.'.$this->Kd_Rek_2.'.'.$this->Kd_Rek_3.'.'.str_pad($this->Kd_Rek_4, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Rek_5, 2, '0', STR_PAD_LEFT).'.'. str_pad($this->No_Rinc, 2, '0', STR_PAD_LEFT);
            },
            'PAGU'  => function(){
                return number_format($this->pagu,0,'','');
            },
            'IS_DELETED' => function(){
                return false;
            },
           'CREATE_TIME' => function(){
                //var_dump($this->CREATED_TIME);exit;
                return strtotime($this->CREATED_TIME);
            },
            'LASTUPDATE_TIME' => function(){
                return strtotime($this->LASTUPDATED_TIME);
            }
        ];
    }

    public function lkpp_anggaran(){
        return [
            'ID' => function(){
                return $this->id_unit;
            },
            'BELANJA_LANGSUNG_PEGAWAI' => function(){
                return number_format($this->BLP,0,'','');
            },
            'BELANJA_LANGSUNG_BUKAN_PEGAWAI' => function(){
                return number_format(0,0,'','');
            },
            'BELANJA_LANGSUNG_BUKAN_PEGAWAI_BARANGJASA' => function(){
                return number_format($this->BLBJ,0,'','');
            },
            'BELANJA_LANGSUNG_BUKAN_PEGAWAI_MODAL' => function(){
                return number_format($this->BLM,0,'','');
            },
            'BELANJA_TIDAK_LANGSUNG_PEGAWAI'  => function(){
                return number_format($this->BTLP,0,'','');
            },
            'BELANJA_TIDAK_LANGSUNG_BUKAN_PEGAWAI' => function(){
                return number_format($this->BTL,0,'','');
            },
            'BELANJA_TIDAK_LANGSUNG_BUKAN_PEGAWAI_BUKAN_PENGADAAN' => function(){
                return number_format(0,0,'','');
            },
            'BELANJA_TIDAK_LANGSUNG_BUKAN_PEGAWAI_PENGADAAN' => function(){
                return number_format(0,0,'','');
            },
            'ID_SATKER'  => function(){
                return $this->Kd_Urusan . '.'.str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Sub, 2, '0', STR_PAD_LEFT);
            },
            'ID_KLDI' => function(){
                return $this->KLDI;
            } 
        ];
    }
}
