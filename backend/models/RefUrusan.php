<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ref_Urusan".
 *
 * @property int $Tahun
 * @property int $Kd_Urusan
 * @property string $Nm_Urusan
 */
class RefUrusan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Urusan';
    }

    public static function getDb()
    {
        // use the "db3" application component
        return \Yii::$app->db_main;  
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Urusan', 'Nm_Urusan'], 'required'],
            [['Tahun', 'Kd_Urusan'], 'integer'],
            [['Nm_Urusan'], 'string'],
            [['Kd_Urusan', 'Tahun'], 'unique', 'targetAttribute' => ['Kd_Urusan', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Urusan' => 'Kd  Urusan',
            'Nm_Urusan' => 'Nm  Urusan',
        ];
    }
}
