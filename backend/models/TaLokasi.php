<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ta_Lokasi".
 *
 * @property int $ID_RUP
 * @property int $ID_Prov
 * @property int $ID_Kab
 * @property string $Detail_Lokasi
 * @property int $Tahun
 */
class TaLokasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Lokasi';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_RUP', 'ID_Prov', 'ID_Kab'], 'required'],
            [['ID_RUP', 'ID_Prov', 'ID_Kab', 'Tahun'], 'integer'],
            [['Detail_Lokasi'], 'string'],
            [['ID_Kab', 'ID_Prov', 'ID_RUP'], 'unique', 'targetAttribute' => ['ID_Kab', 'ID_Prov', 'ID_RUP']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_RUP' => 'Id  Rup',
            'ID_Prov' => 'Id  Prov',
            'ID_Kab' => 'Id  Kab',
            'Detail_Lokasi' => 'Detail  Lokasi',
            'Tahun' => 'Tahun',
        ];
    }
}
