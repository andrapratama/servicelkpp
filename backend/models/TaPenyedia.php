<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ta_Penyedia".
 *
 * @property int $Tahun
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property int $Kd_Prog
 * @property int $ID_Prog
 * @property int $Kd_Keg
 * @property int $ID_RUP
 * @property int $ID_Swakelola
 * @property int $ID_Satker
 * @property string $PPK
 * @property string $SatuanKerja
 * @property string $Nama_Paket
 * @property string $Lokasi_Pekerjaan
 * @property string $Detail_Lokasi_Pekerjaan
 * @property string $Volume
 * @property string $Uraian_Pekerjaan
 * @property string $Spesifikasi
 * @property string $PDN
 * @property string $UsahaKecil
 * @property string $PraDIPA
 * @property string $No_Renja
 * @property string $SumberDana
 * @property string $AsalDana
 * @property string $MAK
 * @property string $Komponen_Kegiatan
 * @property string $Pagu_Keg
 * @property string $IzinTahunJamak
 * @property string $Jenis_Pengadaan
 * @property string $Total_Pagu
 * @property int $Metode_Pengadaan
 * @property string $TglPemanfaatan
 * @property string $Bulan_Pemilihan_Mulai
 * @property string $Bulan_Pemilihan_Akhir
 * @property string $Bulan_Pekerjaan_Mulai
 * @property string $Bulan_Pekerjaan_Akhir
 * @property string $List_Paket_Anggaran
 * @property string $Create_Time
 * @property string $LastUpdate_Time
 * @property int $Aktif
 * @property int $Umumkan
 * @property int $Is_Final
 * @property int $ID_PPK
 * @property int $Is_Deleted
 */
class TaPenyedia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Penyedia';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'ID_RUP'], 'required'],
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'ID_RUP', 'ID_Swakelola', 'ID_Satker', 'PDN', 'UsahaKecil', 'PraDIPA', 'Metode_Pengadaan', 'Aktif', 'Umumkan', 'Is_Final', 'ID_PPK', 'Is_Deleted'], 'integer'],
            [['PPK', 'SatuanKerja', 'Nama_Paket', 'Satuan123', 'Uraian_Pekerjaan', 'Spesifikasi', 'No_Renja', 'Komponen_Kegiatan', 'IzinTahunJamak', 'TglPemanfaatan', 'Bulan_Pemilihan_Mulai', 'Bulan_Pemilihan_Akhir', 'Bulan_Pekerjaan_Mulai', 'Bulan_Pekerjaan_Akhir', 'Create_Time', 'LastUpdate_Time'], 'string'],
            [['Jml_Satuan', 'Total_Pagu'], 'number'],
            [['ID_Prog', 'ID_RUP', 'Kd_Bidang', 'Kd_Keg', 'Kd_Prog', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'Tahun'], 'unique', 'targetAttribute' => ['ID_Prog', 'ID_RUP', 'Kd_Bidang', 'Kd_Keg', 'Kd_Prog', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'Kd_Prog' => 'Kd  Prog',
            'ID_Prog' => 'Id  Prog',
            'Kd_Keg' => 'Kd  Keg',
            'ID_RUP' => 'Id  Rup',
            'ID_Swakelola' => 'Id  Swakelola',
            'ID_Satker' => 'Id  Satker',
            'PPK' => 'Ppk',
            'SatuanKerja' => 'Satuan Kerja',
            'Nama_Paket' => 'Nama  Paket',
            'Satuan123' => 'Satuan123',
            'Jml_Satuan' => 'Jml  Satuan',
            'Uraian_Pekerjaan' => 'Uraian  Pekerjaan',
            'Spesifikasi' => 'Spesifikasi',
            'PDN' => 'Pdn',
            'UsahaKecil' => 'Usaha Kecil',
            'PraDIPA' => 'Pra Dipa',
            'No_Renja' => 'No  Renja',
            'Total_Pagu' => 'Total  Pagu',
            'Komponen_Kegiatan' => 'Komponen  Kegiatan',
            'IzinTahunJamak' => 'Izin Tahun Jamak',
            'Metode_Pengadaan' => 'Metode  Pengadaan',
            'TglPemanfaatan' => 'Tgl Pemanfaatan',
            'Bulan_Pemilihan_Mulai' => 'Bulan  Pemilihan  Mulai',
            'Bulan_Pemilihan_Akhir' => 'Bulan  Pemilihan  Akhir',
            'Bulan_Pekerjaan_Mulai' => 'Bulan  Pekerjaan  Mulai',
            'Bulan_Pekerjaan_Akhir' => 'Bulan  Pekerjaan  Akhir',
            'Create_Time' => 'Create  Time',
            'LastUpdate_Time' => 'Last Update  Time',
            'Aktif' => 'Aktif',
            'Umumkan' => 'Umumkan',
            'Is_Final' => 'Is  Final',
            'ID_PPK' => 'Id  Ppk',
            'Is_Deleted' => 'Is  Deleted',
        ];
    }

    public function fields(){
        switch (Yii::$app->controller->action->uniqueId) {
            case 'lkpp/penyedia':
                return $this->lkpp_penyedia();
                break;
            
            
            default:
                return parent::fields();
                break;
        }
    }

    public function lkpp_penyedia(){
        $arr =  [
            "ID_RUP" => "ID_RUP", // id sequence anda
            "ID_SWAKELOLA" => "ID_Swakelola",
        ];
        $arr['ID_SATKER'] = function(){
            return $this->Kd_Urusan . '.'.str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Sub, 2, '0', STR_PAD_LEFT);
        };
        $arr['NAMA_PAKET'] = "Nama_Paket";
        $arr['LIST_LOKASI_PEKERJAAN'] = function(){
            $res = [];
            $models = TaLokasiPenyedia::find()->where(['ID_RUP' => $this->ID_RUP])->all();
            foreach ($models as $key => $model) {
                $resModel['ID_PROVINSI'] = $model->ID_Prov;
                $resModel['ID_KABUPATEN'] = $model->ID_Kab;
                $resModel['DETIL_LOKASI'] = $model->Detail_Lokasi;
                $res[] = $resModel;
            }
            return $res;
        };
        $arr["VOLUME"] = function(){
            return number_format($this->Jml_Satuan,0,'',''). ' '. $this->Satuan123;
        };
        $arr["URAIAN_PEKERJAAN"] = "Uraian_Pekerjaan";
        $arr["SPESIFIKASI"] = "Spesifikasi";
        $arr["PDN"] = function(){
            return $this->PDN == 1 ? true : false;
        };
        $arr["UMUK"] = function(){
            return $this->UsahaKecil == 1 ? true : false;
        };
        $arr["PRADIPA"] = function(){
            return $this->PraDIPA == 1 ? true : false;
        };
        $arr["NO_RENJA"] = "No_Renja";
        $arr["TOTAL_PAGU"] = function(){
            return number_format($this->Total_Pagu,0,'','');
        };
        $arr["LIST_PAKET_ANGGARAN"] = function(){
            $res = [];
            $models = TaPaketPenyedia::find()->where(['ID_RUP' => $this->ID_RUP, 'Tahun' => $this->Tahun])->all();
            foreach ($models as $key => $model) {
                //$kegiatan = TaKegiatan
                $resModel['TAHUN_ANGGARAN'] = $model->Tahun;
                $resModel['SUMBER_DANA'] = $model->Sumber_Dana;
                $resModel['ASAL_DANA'] = $model->Asal_Dana;
                $resModel['ASAL_DANA_SATKER'] = $this->Kd_Urusan . '.'.str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Sub, 2, '0', STR_PAD_LEFT);
                $resModel['MAK'] = $model->MAK;
                $resModel['pagu'] = number_format($model->Pagu,0,'','');
                $resModel['ID_KEGIATAN'] = TaKegiatanId::find()->where([
                    'Tahun' => $model->Tahun,
                    'Kd_Urusan' => $model->Kd_Urusan,
                    'Kd_Bidang' => $model->Kd_Bidang,
                    'Kd_Unit' => $model->Kd_Unit,
                    'Kd_Sub' => $model->Kd_Sub,
                    'Kd_Prog' => $model->Kd_Prog,
                    'ID_Prog' => $model->ID_Prog,
                    'Kd_Keg' => $model->Kd_Keg,
                ])->one()->id;
                $resModel['ID_RINCI_OBJEK_AKUN'] = TaBelanjaRincId::find()->where([
                    'Tahun' => $model->Tahun,
                    'Kd_Urusan' => $model->Kd_Urusan,
                    'Kd_Bidang' => $model->Kd_Bidang,
                    'Kd_Unit' => $model->Kd_Unit,
                    'Kd_Sub' => $model->Kd_Sub,
                    'Kd_Prog' => $model->Kd_Prog,
                    'ID_Prog' => $model->ID_Prog,
                    'Kd_Keg' => $model->Kd_Keg,
                    'Kd_Rek_1' => $model->Kd_Rek_1,
                    'Kd_Rek_2' => $model->Kd_Rek_2,
                    'Kd_Rek_3' => $model->Kd_Rek_3,
                    'Kd_Rek_4' => $model->Kd_Rek_4,
                    'Kd_Rek_5' => $model->Kd_Rek_5,
                    'No_Rinc' => $model->No_Rinc,
                ])->one()->id;
                $res[] = $resModel;
            }
            return $res;
        };
        $arr["IZIN_TAHUN_JAMAK"] = "IzinTahunJamak";
        $arr["LIST_PAKET_JENIS_PENGADAAN"] = function(){
            $res = [];
            $models = TaJenisPengadaan::find()->where(['ID_RUP' => $this->ID_RUP, 'Tahun' => $this->Tahun])->all();
            foreach ($models as $key => $model) {
                //$kegiatan = TaKegiatan
                $resModel['JENIS_ID'] = $model->Jenis_ID;
                $resModel['JUMLAH_PAGU'] = number_format($model->Jumlah_Pagu,0,'','');
                $res[] = $resModel;
            }
            return $res;
        };
        $arr["METODE_PENGADAAN"] = "Metode_Pengadaan";
        $arr["TANGGAL_KEBUTUHAN"] = function(){
            return strtotime($this->TglPemanfaatan);
        };
        $arr["BULAN_PEKERJAAN_AKHIR"] = function(){
            return strtotime($this->Bulan_Pekerjaan_Akhir);
        };
        $arr["BULAN_PEKERJAAN_MULAI"] = function(){
            return strtotime($this->Bulan_Pekerjaan_Mulai);
        };
        $arr["BULAN_PEMILIHAN_AKHIR"] = function(){
            return strtotime($this->Bulan_Pemilihan_Akhir);
        };
        $arr["BULAN_PEMILIHAN_MULAI"] = function(){
            return strtotime($this->Bulan_Pemilihan_Mulai);
        };
         $arr["TANGGAL_AWAL_KEBUTUHAN"] = function(){
            return $this->Tanggal_Awal_Kebutuhan != null ? strtotime($this->Tanggal_Awal_Kebutuhan) : strtotime($this->TglPemanfaatan);
        };
        $arr["TANGGAL_AKHIR_KEBUTUHAN"] = function(){
            return $this->Tanggal_Akhir_Kebutuhan != null ? strtotime($this->Tanggal_Akhir_Kebutuhan) : strtotime($this->TglPemanfaatan);
        };
        $arr["CREATE_TIME"] = function(){
            return strtotime($this->Create_Time);
        };
        $arr["LASTUPDATE_TIME"] =  function(){
            return $this->LastUpdate_Time != null ? strtotime($this->LastUpdate_Time) : strtotime($this->Create_Time);
        };
        $arr["AKTIF"] = function(){
            return $this->Aktif == 1 ? true : false;
        };
        $arr["UMUMKAN"] = function(){
            return $this->Umumkan == 1 ? true : false;
        };
        $arr["IS_FINAL"] = function(){
            return $this->Is_Final == 1 ? true : false;
        };
        $arr["ID_PPK"] = "ID_PPK";
        $arr["IS_DELETED"] = function(){
            return $this->Is_Deleted== 1 ? true : false;
        };;
        
        
        return $arr;
    }
}
