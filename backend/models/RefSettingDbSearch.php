<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RefSettingDb;

/**
 * RefSettingDbSearch represents the model behind the search form about `backend\models\RefSettingDb`.
 */
class RefSettingDbSearch extends RefSettingDb
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'port', 'waktu_interval'], 'integer'],
            [['nm_setting_db', 'ip', 'username', 'password', 'db_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefSettingDb::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'port' => $this->port,
            'waktu_interval' => $this->waktu_interval,
        ]);

        $query->andFilterWhere(['like', 'nm_setting_db', $this->nm_setting_db])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'db_name', $this->db_name]);

        return $dataProvider;
    }
}
