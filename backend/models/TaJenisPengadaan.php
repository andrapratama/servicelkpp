<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ta_Jenis_Pengadaan".
 *
 * @property int $Tahun
 * @property int $ID_RUP
 * @property int $Jenis_ID
 * @property string $Jumlah_Pagu
 */
class TaJenisPengadaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Jenis_Pengadaan';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'ID_RUP', 'Jenis_ID'], 'required'],
            [['Tahun', 'ID_RUP', 'Jenis_ID'], 'integer'],
            [['Jumlah_Pagu'], 'number'],
            [['ID_RUP', 'Jenis_ID', 'Tahun'], 'unique', 'targetAttribute' => ['ID_RUP', 'Jenis_ID', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'ID_RUP' => 'Id  Rup',
            'Jenis_ID' => 'Jenis  ID',
            'Jumlah_Pagu' => 'Jumlah  Pagu',
        ];
    }
}
