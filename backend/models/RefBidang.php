<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ref_Bidang".
 *
 * @property int $Tahun
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property string $Nm_Bidang
 * @property int $Kd_Fungsi
 */
class RefBidang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Bidang';
    }

    public static function getDb()
    {
        // use the "db3" application component
        return \Yii::$app->db_main;  
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Nm_Bidang', 'Kd_Fungsi'], 'required'],
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Fungsi'], 'integer'],
            [['Nm_Bidang'], 'string'],
            [['Kd_Bidang', 'Kd_Urusan', 'Tahun'], 'unique', 'targetAttribute' => ['Kd_Bidang', 'Kd_Urusan', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Nm_Bidang' => 'Nm  Bidang',
            'Kd_Fungsi' => 'Kd  Fungsi',
        ];
    }
}
