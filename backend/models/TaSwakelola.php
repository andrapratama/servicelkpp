<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ta_Swakelola".
 *
 * @property int $Tahun
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property int $Kd_Prog
 * @property int $ID_Prog
 * @property int $Kd_Keg
 * @property int $ID_RUP
 * @property int $ID_Satker
 * @property int $Tipe_Swakelola
 * @property string $Nama_KLDP_Lain
 * @property string $Nama_Satker_Lain
 * @property string $List_Lokasi_Pekerjaan
 * @property string $Volume
 * @property string $Uraian_Pekerjaan
 * @property string $Total_Pagu
 * @property string $List_Paket_Anggaran
 * @property string $Bulan_Pekerjaan_Mulai
 * @property string $Bulan_Pekerjaan_Akhir
 * @property string $Create_Time
 * @property string $LastUpdate_Time
 * @property string $Aktif
 * @property string $Umumkan
 * @property int $Is_Final
 * @property int $ID_PPK
 * @property int $Is_Deleted
 * @property int $ID_Kegiatan
 * @property string $Komponen_Kegiatan
 * @property string $Nama_Paket
 * @property string $SumberDana
 * @property string $SatuanKerja
 * @property string $AsalDana
 * @property string $Penyelenggara_Swakelola
 * @property string $MAK
 * @property string $PraDipa
 */
class TaSwakelola extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Swakelola';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'ID_RUP'], 'required'],
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'ID_RUP', 'ID_Satker', 'Tipe_Swakelola', 'PraDipa', 'Is_Final', 'ID_PPK', 'Is_Deleted'], 'integer'],
            [['Nama_KLDP_Lain', 'Nama_Satker_Lain', 'Penyelenggara_Swakelola', 'SatuanKerja', 'Nama_Paket', 'Satuan123', 'Uraian_Pekerjaan', 'Komponen_Kegiatan', 'Bulan_Pekerjaan_Mulai', 'Bulan_Pekerjaan_Akhir', 'Create_Time', 'LastUpdate_Time', 'Aktif', 'Umumkan'], 'string'],
            [['Jml_Satuan', 'Total_Pagu'], 'number'],
            [['ID_Prog', 'ID_RUP', 'Kd_Bidang', 'Kd_Keg', 'Kd_Prog', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'Tahun'], 'unique', 'targetAttribute' => ['ID_Prog', 'ID_RUP', 'Kd_Bidang', 'Kd_Keg', 'Kd_Prog', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'Kd_Prog' => 'Kd  Prog',
            'ID_Prog' => 'Id  Prog',
            'Kd_Keg' => 'Kd  Keg',
            'ID_RUP' => 'Id  Rup',
            'ID_Satker' => 'Id  Satker',
            'Tipe_Swakelola' => 'Tipe  Swakelola',
            'Nama_KLDP_Lain' => 'Nama  Kldp  Lain',
            'Nama_Satker_Lain' => 'Nama  Satker  Lain',
            'Penyelenggara_Swakelola' => 'Penyelenggara  Swakelola',
            'SatuanKerja' => 'Satuan Kerja',
            'Nama_Paket' => 'Nama  Paket',
            'Satuan123' => 'Satuan123',
            'Jml_Satuan' => 'Jml  Satuan',
            'Uraian_Pekerjaan' => 'Uraian  Pekerjaan',
            'PraDipa' => 'Pra Dipa',
            'Total_Pagu' => 'Total  Pagu',
            'Komponen_Kegiatan' => 'Komponen  Kegiatan',
            'Bulan_Pekerjaan_Mulai' => 'Bulan  Pekerjaan  Mulai',
            'Bulan_Pekerjaan_Akhir' => 'Bulan  Pekerjaan  Akhir',
            'Create_Time' => 'Create  Time',
            'LastUpdate_Time' => 'Last Update  Time',
            'Aktif' => 'Aktif',
            'Umumkan' => 'Umumkan',
            'Is_Final' => 'Is  Final',
            'ID_PPK' => 'Id  Ppk',
            'Is_Deleted' => 'Is  Deleted',
        ];
    }

    public function fields(){
        switch (Yii::$app->controller->action->uniqueId) {
            case 'lkpp/swakelola':
                return $this->lkpp_swakelola();
                break;
            
            
            default:
                return parent::fields();
                break;
        }
    }

    public function lkpp_swakelola(){
        $arr =  [
            "ID_RUP" => "ID_RUP", 
        ];
        $arr['ID_SATKER'] = function(){
            return $this->Kd_Urusan . '.'.str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Sub, 2, '0', STR_PAD_LEFT);
        };
        $arr["TIPE_SWAKELOLA"] = "Tipe_Swakelola";
        $arr["NAMA_KLPD_LAIN"] = "Nama_KLDP_Lain";
        $arr["NAMA_SATKER_LAIN"] = "Nama_Satker_Lain";
        $arr['NAMA_PAKET'] = "Nama_Paket";
        $arr['LIST_LOKASI_PEKERJAAN'] = function(){
            $res = [];
            $models = TaLokasiSwakelola::find()->where(['ID_RUP' => $this->ID_RUP])->all();
            foreach ($models as $key => $model) {
                $resModel['ID_PROVINSI'] = $model->ID_Prov;
                $resModel['ID_KABUPATEN'] = $model->ID_Kab;
                $resModel['DETIL_LOKASI'] = $model->Detail_Lokasi;
                $res[] = $resModel;
            }
            return $res;
        };
        $arr["VOLUME"] = function(){
            return number_format($this->Jml_Satuan,0,'',''). ' '. $this->Satuan123;
        };
        $arr["URAIAN_PEKERJAAN"] = "Uraian_Pekerjaan";
       /* $arr["SPESIFIKASI"] = "Spesifikasi";
        $arr["PDN"] = function(){
            return $this->PDN == 'TRUE' ? true : false;
        };
        $arr["UMUK"] = function(){
            return $this->Umumkan == 1 ? true : false;
        };
        $arr["PRADIPA"] = function(){
            return $this->PraDIPA == 'TRUE' ? true : false;
        };
        $arr["NO_RENJA"] = "No_Renja";*/
        $arr["TOTAL_PAGU"] = function(){
            return number_format($this->Total_Pagu,0,'','');
        };
        $arr["LIST_PAKET_ANGGARAN"] = function(){
            $res = [];
            $models = TaPaketSwakelola::find()->where(['ID_RUP' => $this->ID_RUP])->all();
            foreach ($models as $key => $model) {
                //$kegiatan = TaKegiatan
                $resModel['TAHUN_ANGGARAN'] = $model->Tahun;
                $resModel['SUMBER_DANA'] = $model->Sumber_Dana;
                $resModel['ASAL_DANA'] = $model->Asal_Dana;
                $resModel['ASAL_DANA_SATKER'] =  $this->Kd_Urusan . '.'.str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Sub, 2, '0', STR_PAD_LEFT);
            
                $resModel['MAK'] = $model->MAK;
                $resModel['pagu'] = number_format($model->Pagu,0,'','');
                $resModel['ID_KEGIATAN'] = TaKegiatanId::find()->where([
                    'Tahun' => $model->Tahun,
                    'Kd_Urusan' => $model->Kd_Urusan,
                    'Kd_Bidang' => $model->Kd_Bidang,
                    'Kd_Unit' => $model->Kd_Unit,
                    'Kd_Sub' => $model->Kd_Sub,
                    'Kd_Prog' => $model->Kd_Prog,
                    'ID_Prog' => $model->ID_Prog,
                    'Kd_Keg' => $model->Kd_Keg,
                ])->one()->id;
                $resModel['ID_RINCI_OBJEK_AKUN'] = TaBelanjaRincId::find()->where([
                    'Tahun' => $model->Tahun,
                    'Kd_Urusan' => $model->Kd_Urusan,
                    'Kd_Bidang' => $model->Kd_Bidang,
                    'Kd_Unit' => $model->Kd_Unit,
                    'Kd_Sub' => $model->Kd_Sub,
                    'Kd_Prog' => $model->Kd_Prog,
                    'ID_Prog' => $model->ID_Prog,
                    'Kd_Keg' => $model->Kd_Keg,
                    'Kd_Rek_1' => $model->Kd_Rek_1,
                    'Kd_Rek_2' => $model->Kd_Rek_2,
                    'Kd_Rek_3' => $model->Kd_Rek_3,
                    'Kd_Rek_4' => $model->Kd_Rek_4,
                    'Kd_Rek_5' => $model->Kd_Rek_5,
                    'No_Rinc' => $model->No_Rinc,
                ])->one()->id;
                $res[] = $resModel;
            }
            return $res;
        };
        /*$arr["METODE_PENGADAAN"] = "Metode_Pengadaan";
        $arr["TANGGAL_KEBUTUHAN"] = "TglPemanfaatan";*/
        $arr["BULAN_PEKERJAAN_AKHIR"] = function(){
            return strtotime($this->Bulan_Pekerjaan_Akhir);
        };
        $arr["BULAN_PEKERJAAN_MULAI"] = function(){
            return strtotime($this->Bulan_Pekerjaan_Mulai);
        };
        /*$arr["BULAN_PEMILIHAN_AKHIR"] = "Bulan_Pemilihan_Akhir";
        $arr["BULAN_PEMILIHAN_MULAI"] = "Bulan_Pemilihan_Mulai";*/
        $arr["CREATE_TIME"] = function(){
            return strtotime($this->Create_Time);
        };
        $arr["LASTUPDATE_TIME"] =  function(){
            return $this->LastUpdate_Time != null ? strtotime($this->LastUpdate_Time) : strtotime($this->Create_Time);
        };
        $arr["AKTIF"] = function(){
            return $this->Aktif == 1 ? true : false;
        };
        $arr["UMUMKAN"] = function(){
            return $this->Umumkan == 1 ? true : false;
        };
        $arr["IS_FINAL"] = function(){
            return $this->Is_Final == 1 ? true : false;
        };
        $arr["ID_PPK"] = "ID_PPK";
        $arr["IS_DELETED"] = function(){
            return $this->Is_Deleted== 1 ? true : false;
        };
	//var_dump($arr["ID_PPK"]);exit;	
	//$arr["ID_KEGIATAN"] = "1";        
	$arr["ID_KEGIATAN"] = function(){
            return TaKegiatanId::find()->where([
                    'Tahun' => $this->Tahun,
                    'Kd_Urusan' => $this->Kd_Urusan,
                    'Kd_Bidang' => $this->Kd_Bidang,
                    'Kd_Unit' => $this->Kd_Unit,
                    'Kd_Sub' => $this->Kd_Sub,
                    'Kd_Prog' => $this->Kd_Prog,
                    'ID_Prog' => $this->ID_Prog,
                    'Kd_Keg' => $this->Kd_Keg,
                    
                ])->one()->id;
        };
        return $arr;
    }
}
