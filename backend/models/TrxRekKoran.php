<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "TrxRekKoran".
 *
 * @property int $CHANNELID
 * @property string $ACCNBR
 * @property string $TXDTSTLMN
 * @property string $TXAMT
 * @property string $TXBRANCH
 * @property int $DBCR
 * @property string $TXMSG
 * @property string $TXID
 * @property string $USERID
 * @property string $TXDATE
 * @property string $TXCCY
 * @property string $TXCODE
 */
class TrxRekKoran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TrxRekKoran';
    }

    public static function getDb(){
        return \Yii::$app->db_main;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CHANNELID', 'ACCNBR', 'TXDTSTLMN', 'TXAMT', 'TXBRANCH', 'DBCR', 'TXMSG', 'TXID', 'USERID', 'TXDATE', 'TXCCY', 'TXCODE'], 'required'],
            [['CHANNELID', 'DBCR'], 'integer'],
            [['ACCNBR', 'TXBRANCH', 'TXMSG', 'TXID', 'USERID', 'TXCCY', 'TXCODE'], 'string'],
            [['TXDTSTLMN', 'TXDATE'], 'safe'],
            [['TXAMT'], 'number'],
            [['DBCR', 'TXDTSTLMN', 'TXID'], 'unique', 'targetAttribute' => ['DBCR', 'TXDTSTLMN', 'TXID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CHANNELID' => 'Channelid',
            'ACCNBR' => 'Accnbr',
            'TXDTSTLMN' => 'Txdtstlmn',
            'TXAMT' => 'Txamt',
            'TXBRANCH' => 'Txbranch',
            'DBCR' => 'Dbcr',
            'TXMSG' => 'Txmsg',
            'TXID' => 'Txid',
            'USERID' => 'Userid',
            'TXDATE' => 'Txdate',
            'TXCCY' => 'Txccy',
            'TXCODE' => 'Txcode',
        ];
    }
}
