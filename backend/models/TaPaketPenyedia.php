<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ta_Paket_Penyedia".
 *
 * @property int $Tahun
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property int $Kd_Prog
 * @property int $ID_Prog
 * @property int $Kd_Keg
 * @property int $Kd_Rek_1
 * @property int $Kd_Rek_2
 * @property int $Kd_Rek_3
 * @property int $Kd_Rek_4
 * @property int $Kd_Rek_5
 * @property int $No_Rinc
 * @property int $No_ID
 * @property int $ID_RUP
 * @property int $Sumber_Dana
 * @property string $Asal_Dana
 * @property string $Asal_Dana_Satker
 * @property string $MAK
 * @property string $Pagu
 */
class TaPaketPenyedia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Paket_Penyedia';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'No_Rinc', 'No_ID', 'ID_RUP'], 'required'],
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'No_Rinc', 'No_ID', 'ID_RUP', 'Sumber_Dana'], 'integer'],
            [['Asal_Dana', 'Asal_Dana_Satker', 'MAK'], 'string'],
            [['Pagu'], 'number'],
            [['ID_Prog', 'ID_RUP', 'Kd_Bidang', 'Kd_Keg', 'Kd_Prog', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'No_ID', 'No_Rinc', 'Tahun'], 'unique', 'targetAttribute' => ['ID_Prog', 'ID_RUP', 'Kd_Bidang', 'Kd_Keg', 'Kd_Prog', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'No_ID', 'No_Rinc', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'Kd_Prog' => 'Kd  Prog',
            'ID_Prog' => 'Id  Prog',
            'Kd_Keg' => 'Kd  Keg',
            'Kd_Rek_1' => 'Kd  Rek 1',
            'Kd_Rek_2' => 'Kd  Rek 2',
            'Kd_Rek_3' => 'Kd  Rek 3',
            'Kd_Rek_4' => 'Kd  Rek 4',
            'Kd_Rek_5' => 'Kd  Rek 5',
            'No_Rinc' => 'No  Rinc',
            'No_ID' => 'No  ID',
            'ID_RUP' => 'Id  Rup',
            'Sumber_Dana' => 'Sumber  Dana',
            'Asal_Dana' => 'Asal  Dana',
            'Asal_Dana_Satker' => 'Asal  Dana  Satker',
            'MAK' => 'Mak',
            'Pagu' => 'Pagu',
        ];
    }
}
