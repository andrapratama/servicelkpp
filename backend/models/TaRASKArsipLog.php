<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ta_RASK_Arsip_Log".
 *
 * @property string $User_ID
 * @property int $Tahun
 * @property int $Kd_Perubahan
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property int $Kd_Prog
 * @property int $Kd_Keg
 * @property string $Keterangan
 * @property string $Tgl_Posting
 */
class TaRASKArsipLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_RASK_Arsip_Log';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['User_ID', 'Kd_Perubahan', 'Tgl_Posting'], 'required'],
            [['User_ID', 'Kd_Perubahan', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'Kd_Keg', 'Keterangan'], 'string'],
            [['Tahun'], 'integer'],
            [['Tgl_Posting'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'User_ID' => 'User  ID',
            'Tahun' => 'Tahun',
            'Kd_Perubahan' => 'Kd  Perubahan',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'Kd_Prog' => 'Kd  Prog',
            'Kd_Keg' => 'Kd  Keg',
            'Keterangan' => 'Keterangan',
            'Tgl_Posting' => 'Tgl  Posting',
        ];
    }
}
