<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ref_Verifikasi_Asn".
 *
 * @property string $nip
 * @property string $nama
 * @property string $pangkat
 * @property string $jenis_jabatan
 * @property string $nama_jabatan
 * @property string $tmt_jabatan
 * @property string $foto
 * @property string $alamat
 * @property string $email
 * @property int $status_kepegawaian
 * @property string $oncreated
 * @property string $onupdated
 * @property string $golongan
 */
class RefVerifikasiAsn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Verifikasi_Asn';
    }

    public static function getDb()
    {
        // use the "db3" application component
        return \Yii::$app->db_main;  
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nip'], 'required'],
            [['nip', 'nama', 'pangkat', 'jenis_jabatan', 'nama_jabatan', 'foto', 'alamat', 'email', 'golongan'], 'string'],
            [['tmt_jabatan', 'oncreated', 'onupdated'], 'safe'],
            [['status_kepegawaian'], 'integer'],
            [['nip'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nip' => 'Nip',
            'nama' => 'Nama',
            'pangkat' => 'Pangkat',
            'jenis_jabatan' => 'Jenis Jabatan',
            'nama_jabatan' => 'Nama Jabatan',
            'tmt_jabatan' => 'Tmt Jabatan',
            'foto' => 'Foto',
            'alamat' => 'Alamat',
            'email' => 'Email',
            'status_kepegawaian' => 'Status Kepegawaian',
            'oncreated' => 'Oncreated',
            'onupdated' => 'Onupdated',
            'golongan' => 'Golongan',
        ];
    }
}
