<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ref_Api".
 *
 * @property int $id
 * @property string $id_name
 * @property string $tipe
 * @property string $link
 * @property string $header
 * @property string $body
 * @property string $token
 * @property string $username
 * @property string $pwd
 * @property int $tgltoken
 * @property int $masatoken
 */
class RefApi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Api';
    }

    public static function getDb()
    {
        return \Yii::$app->db_main;  
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_name', 'tipe', 'link', 'header', 'body', 'token', 'username', 'pwd'], 'string'],
            [['tgltoken', 'masatoken'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_name' => 'Id Name',
            'tipe' => 'Tipe',
            'link' => 'Link',
            'header' => 'Header',
            'body' => 'Body',
            'token' => 'Token',
            'username' => 'Username',
            'pwd' => 'Pwd',
            'tgltoken' => 'Tgltoken',
            'masatoken' => 'Masatoken',
        ];
    }
}
