<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ref_Setting".
 *
 * @property int $Tahun
 * @property string $SistemKuitansi
 * @property string $StandardHarga
 * @property string $Kontrol_Angg_SPD
 * @property string $Kontrol_SPD_SPP
 * @property string $Kontrol_SPP_SPM
 * @property int $Locked
 * @property string $LastDBAplVer
 * @property int $DefaultPaper
 * @property int $SPDKegiatan
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property int $Kd_Pembayaran
 * @property string $PFK
 * @property int $Peny_SPJ
 * @property int $SP2DPre
 * @property string $SP2DFormat
 * @property int $KunciPagu
 * @property string $Prognosis
 * @property int $Akrual
 * @property int $Kd_Perubahan_SPD_SPP
 * @property int $Sinkronisasi_Pkeras
 * @property string $Link_Pkeras
 * @property int $LastUpdate_Pkeras
 * @property int $Token_Pkeras
 * @property string $Token_Pkeras
 */
class RefSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'SPDKegiatan', 'PFK', 'Peny_SPJ', 'KunciPagu', 'Prognosis', 'Akrual'], 'required'],
            [['Tahun', 'Locked', 'DefaultPaper', 'SPDKegiatan', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Pembayaran', 'Peny_SPJ', 'SP2DPre', 'KunciPagu', 'Akrual', 'Kd_Perubahan_SPD_SPP', 'Sinkronisasi_Pkeras', 'LastUpdate_Pkeras'], 'integer'],
            [['SistemKuitansi', 'StandardHarga', 'Kontrol_Angg_SPD', 'Kontrol_SPD_SPP', 'Kontrol_SPP_SPM', 'LastDBAplVer', 'PFK', 'SP2DFormat', 'Prognosis', 'Link_Pkeras', 'Token_Pkeras'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'SistemKuitansi' => 'Sistem Kuitansi',
            'StandardHarga' => 'Standard Harga',
            'Kontrol_Angg_SPD' => 'Kontrol  Angg  Spd',
            'Kontrol_SPD_SPP' => 'Kontrol  Spd  Spp',
            'Kontrol_SPP_SPM' => 'Kontrol  Spp  Spm',
            'Locked' => 'Locked',
            'LastDBAplVer' => 'Last Dbapl Ver',
            'DefaultPaper' => 'Default Paper',
            'SPDKegiatan' => 'Spdkegiatan',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'Kd_Pembayaran' => 'Kd  Pembayaran',
            'PFK' => 'Pfk',
            'Peny_SPJ' => 'Peny  Spj',
            'SP2DPre' => 'Sp2 Dpre',
            'SP2DFormat' => 'Sp2 Dformat',
            'KunciPagu' => 'Kunci Pagu',
            'Prognosis' => 'Prognosis',
            'Akrual' => 'Akrual',
            'Kd_Perubahan_SPD_SPP' => 'Kd  Perubahan  Spd  Spp',
            'Sinkronisasi_Pkeras' => 'Sinkronisasi  Pkeras',
            'Link_Pkeras' => 'Link  Pkeras',
            'LastUpdate_Pkeras' => 'Last Update  Pkeras',
            'Token_Pkeras' => 'Token Perangkat Keras'
        ];
    }
}
