<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ta_Revisi_Paket".
 *
 * @property int $ID
 * @property int $ID_RUP
 * @property int $ID_RUP_TO
 * @property string $JENIS
 * @property string $TIPE
 * @property string $ALASAN_REVISI
 * @property string $CREATE_TIME
 * @property int $Tahun
 */
class TaRevisiPaket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Revisi_Paket';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'ID_RUP', 'ID_RUP_TO', 'Tahun'], 'integer'],
            [['JENIS', 'TIPE', 'ALASAN_REVISI'], 'string'],
            [['CREATE_TIME'], 'safe'],
            [['ID'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_RUP' => 'Id  Rup',
            'ID_RUP_TO' => 'Id  Rup  To',
            'JENIS' => 'Jenis',
            'TIPE' => 'Tipe',
            'ALASAN_REVISI' => 'Alasan  Revisi',
            'CREATE_TIME' => 'Create  Time',
            'Tahun' => 'Tahun',
        ];
    }

    public function fields(){
        switch (Yii::$app->controller->action->uniqueId) {
            case 'lkpp/revisipaket':
                return $this->lkpp_revisi();
                break;
            
            
            default:
                return parent::fields();
                break;
        }
    }

    public function lkpp_revisi(){
        $arr = [
            'ID' => 'ID', 
            'ID_RUP' => 'ID_RUP', 
            'ID_RUP_TO' => 'ID_RUP_TO', 
            'JENIS' => 'JENIS', 
            'TIPE' => 'TIPE', 
            'ALASAN_REVISI' => 'ALASAN_REVISI', 
        ];
        $arr['CREATE_TIME'] = function(){
            return strtotime($this->CREATE_TIME);
        };
        return $arr;
    }
}
