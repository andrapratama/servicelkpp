<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ta_Lokasi_Penyedia".
 *
 * @property int $Tahun
 * @property int $ID_RUP
 * @property int $ID_Prov
 * @property int $ID_Kab
 * @property string $Detail_Lokasi
 */
class TaLokasiPenyedia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Lokasi_Penyedia';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'ID_RUP', 'ID_Prov', 'ID_Kab'], 'required'],
            [['Tahun', 'ID_RUP', 'ID_Prov', 'ID_Kab'], 'integer'],
            [['Detail_Lokasi'], 'string'],
            [['ID_Kab', 'ID_Prov', 'ID_RUP', 'Tahun'], 'unique', 'targetAttribute' => ['ID_Kab', 'ID_Prov', 'ID_RUP', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'ID_RUP' => 'Id  Rup',
            'ID_Prov' => 'Id  Prov',
            'ID_Kab' => 'Id  Kab',
            'Detail_Lokasi' => 'Detail  Lokasi',
        ];
    }
}
