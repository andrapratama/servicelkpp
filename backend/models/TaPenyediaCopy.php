<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ta_Penyedia".
 *
 * @property int $Tahun
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property int $Kd_Prog
 * @property int $ID_Prog
 * @property int $Kd_Keg
 * @property int $ID_RUP
 * @property int $ID_Swakelola
 * @property int $ID_Satker
 * @property string $PPK
 * @property string $SatuanKerja
 * @property string $Nama_Paket
 * @property string $Satuan123
 * @property string $Jml_Satuan
 * @property string $Uraian_Pekerjaan
 * @property string $Spesifikasi
 * @property int $PDN
 * @property int $UsahaKecil
 * @property int $PraDIPA
 * @property string $No_Renja
 * @property string $Total_Pagu
 * @property string $Komponen_Kegiatan
 * @property string $IzinTahunJamak
 * @property int $Metode_Pengadaan
 * @property string $TglPemanfaatan
 * @property string $Bulan_Pemilihan_Mulai
 * @property string $Bulan_Pemilihan_Akhir
 * @property string $Bulan_Pekerjaan_Mulai
 * @property string $Bulan_Pekerjaan_Akhir
 * @property string $Create_Time
 * @property string $LastUpdate_Time
 * @property int $Aktif
 * @property int $Umumkan
 * @property int $Is_Final
 * @property int $ID_PPK
 * @property int $Is_Deleted
 */
class TaPenyediaCopy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Penyedia';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'ID_RUP'], 'required'],
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'ID_RUP', 'ID_Swakelola', 'ID_Satker', 'PDN', 'UsahaKecil', 'PraDIPA', 'Metode_Pengadaan', 'Aktif', 'Umumkan', 'Is_Final', 'ID_PPK', 'Is_Deleted'], 'integer'],
            [['PPK', 'SatuanKerja', 'Nama_Paket', 'Satuan123', 'Uraian_Pekerjaan', 'Spesifikasi', 'No_Renja', 'Komponen_Kegiatan', 'IzinTahunJamak', 'TglPemanfaatan', 'Bulan_Pemilihan_Mulai', 'Bulan_Pemilihan_Akhir', 'Bulan_Pekerjaan_Mulai', 'Bulan_Pekerjaan_Akhir', 'Create_Time', 'LastUpdate_Time'], 'string'],
            [['Jml_Satuan', 'Total_Pagu'], 'number'],
            [['ID_Prog', 'ID_RUP', 'Kd_Bidang', 'Kd_Keg', 'Kd_Prog', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'Tahun'], 'unique', 'targetAttribute' => ['ID_Prog', 'ID_RUP', 'Kd_Bidang', 'Kd_Keg', 'Kd_Prog', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'Kd_Prog' => 'Kd  Prog',
            'ID_Prog' => 'Id  Prog',
            'Kd_Keg' => 'Kd  Keg',
            'ID_RUP' => 'Id  Rup',
            'ID_Swakelola' => 'Id  Swakelola',
            'ID_Satker' => 'Id  Satker',
            'PPK' => 'Ppk',
            'SatuanKerja' => 'Satuan Kerja',
            'Nama_Paket' => 'Nama  Paket',
            'Satuan123' => 'Satuan123',
            'Jml_Satuan' => 'Jml  Satuan',
            'Uraian_Pekerjaan' => 'Uraian  Pekerjaan',
            'Spesifikasi' => 'Spesifikasi',
            'PDN' => 'Pdn',
            'UsahaKecil' => 'Usaha Kecil',
            'PraDIPA' => 'Pra Dipa',
            'No_Renja' => 'No  Renja',
            'Total_Pagu' => 'Total  Pagu',
            'Komponen_Kegiatan' => 'Komponen  Kegiatan',
            'IzinTahunJamak' => 'Izin Tahun Jamak',
            'Metode_Pengadaan' => 'Metode  Pengadaan',
            'TglPemanfaatan' => 'Tgl Pemanfaatan',
            'Bulan_Pemilihan_Mulai' => 'Bulan  Pemilihan  Mulai',
            'Bulan_Pemilihan_Akhir' => 'Bulan  Pemilihan  Akhir',
            'Bulan_Pekerjaan_Mulai' => 'Bulan  Pekerjaan  Mulai',
            'Bulan_Pekerjaan_Akhir' => 'Bulan  Pekerjaan  Akhir',
            'Create_Time' => 'Create  Time',
            'LastUpdate_Time' => 'Last Update  Time',
            'Aktif' => 'Aktif',
            'Umumkan' => 'Umumkan',
            'Is_Final' => 'Is  Final',
            'ID_PPK' => 'Id  Ppk',
            'Is_Deleted' => 'Is  Deleted',
        ];
    }
}
