<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ta_PPK".
 *
 * @property int $Tahun
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property int $ID
 * @property string $Nama
 * @property string $Jabatan
 * @property string $Alamat
 * @property int $Status_Pengguna
 * @property string $NIP
 * @property string $NRP
 * @property string $NIK
 * @property string $Golongan
 * @property string $No_Telepon
 * @property string $Email
 * @property string $No_SK
 * @property string $Created_Time
 * @property string $LastUpdate_Time
 */
class TaPPK extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    

    public static function tableName()
    {
        return 'Ta_PPK';
    }

    public static function getDb(){
        return Yii::$app->db_main;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'ID'], 'integer'],
            [['Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub'], 'required'],
            [['Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Nama', 'Jabatan', 'Alamat', 'Status_Pengguna', 'NIP', 'NRP', 'NIK', 'Golongan', 'No_Telepon', 'Email', 'No_SK'], 'string'],
            [['Created_Time', 'LastUpdate_Time'], 'safe'],
            [['Kd_Bidang', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan'], 'unique', 'targetAttribute' => ['Kd_Bidang', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan']],
        ];
    }

    public function fields(){
        switch (Yii::$app->controller->action->uniqueId) {
            case 'lkpp/ppk':
                return $this->lkpp_ppk();
                break;
            
            
            default:
                return parent::fields();
                break;
        }
    }

    public function lkpp_ppk(){
        $arr =  [
            "ID" => "ID", // id sequence anda
            "NAMA" => "Nama",
            "JABATAN" => "Jabatan",
            "ALAMAT" => "Alamat",
            "STATUS_PENGGUNA" =>  "Status_Pengguna", //1.PNS, 2.Non PNS(NIK), 3. TNI/POLRI (NRP)
           
        ];
        if ($this->NIP != NULL)
            $arr['NIP'] = "NIP";
        if ($this->NRP != NULL)
            $arr['NRP'] = "NRP";
        if ($this->NIK != NULL)
            $arr['NIK'] = "NIK";
        $arr["GOLONGAN"] = "Golongan";
        $arr["NO_TELEPON"] = "No_Telepon";
        $arr["EMAIL"] = "Email";
        $arr["NO_SK"] = "No_SK";
        $arr["CREATED_TIME"] =  function(){
            return strtotime($this->Create_Time);
        };
        $arr["LASTUPDATE_TIME"] =  function(){
            return $this->LastUpdate_Time == null ? strtotime($this->Create_Time) : strtotime($this->LastUpdate_Time);
        };
        $arr["IS_DELETED"] = function (){
            return false;
        };
        $arr['ID_SATKER'] = function(){
            return $this->Kd_Urusan . '.'.str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . '.'.str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). '.'.str_pad($this->Kd_Sub, 2, '0', STR_PAD_LEFT);
        };
        return $arr;
    }

    public static function is_deleted(){
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'ID' => 'ID',
            'Nama' => 'Nama',
            'Jabatan' => 'Jabatan',
            'Alamat' => 'Alamat',
            'Status_Pengguna' => 'Status  Pengguna',
            'NIP' => 'Nip',
            'NRP' => 'Nrp',
            'NIK' => 'Nik',
            'Golongan' => 'Golongan',
            'No_Telepon' => 'No  Telepon',
            'Email' => 'Email',
            'No_SK' => 'No  Sk',
            'Created_Time' => 'Created  Time',
            'LastUpdate_Time' => 'Last Update  Time',
        ];
    }
}
