<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ref_Setting_Db".
 *
 * @property integer $ID
 * @property string $nm_setting_db
 * @property string $ip
 * @property integer $port
 * @property string $username
 * @property string $password
 * @property string $db_name
 * @property integer $waktu_interval
 */
class RefSettingDb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Setting_Db';
    }
    
    public static function getDb()
    {
        // use the "db2" application component
        return \Yii::$app->db_user;  
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nm_setting_db', 'ip', 'port', 'username', 'password', 'db_name', 'koneksi'], 'required'],
            [['ID', 'port', 'waktu_interval'], 'integer'],
            [['nm_setting_db', 'ip', 'username', 'password', 'db_name', 'koneksi'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'nm_setting_db' => 'Nm Setting Db',
            'ip' => 'Ip',
            'port' => 'Port',
            'username' => 'Username',
            'password' => 'Password',
            'db_name' => 'Db Name',
            'waktu_interval' => 'Waktu Interval',
        ];
    }
}
