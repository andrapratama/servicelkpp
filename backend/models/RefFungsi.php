<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ref_Fungsi".
 *
 * @property int $Kd_Fungsi
 * @property string $Nm_Fungsi
 * @property int $Tahun
 */
class RefFungsi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Fungsi';
    }

    public static function getDb()
    {
        // use the "db3" application component
        return \Yii::$app->db_main;  
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Fungsi', 'Nm_Fungsi'], 'required'],
            [['Kd_Fungsi', 'Tahun'], 'integer'],
            [['Nm_Fungsi'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Fungsi' => 'Kd  Fungsi',
            'Nm_Fungsi' => 'Nm  Fungsi',
            'Tahun' => 'Tahun',
        ];
    }
}
