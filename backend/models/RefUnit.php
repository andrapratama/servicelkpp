<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ref_Unit".
 *
 * @property integer $id_unit
 * @property integer $Kd_Urusan
 * @property integer $Kd_Bidang
 * @property integer $Kd_Unit
 * @property string $Nm_Unit
 *
 * @property RefSubUnit[] $refSubUnits
 * @property RefBidang $kdUrusan
 */
class RefUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Unit';
    }
    
    public static function getDb()
    {
        // use the "db3" application component
        return \Yii::$app->db_main;  
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_unit', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Nm_Unit'], 'required'],
            [['id_unit', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit'], 'integer'],
            [['Nm_Unit'], 'string'],
            [['Kd_Urusan'], 'exist', 'skipOnError' => true, 'targetClass' => RefBidang::className(), 'targetAttribute' => ['Kd_Urusan' => 'Kd_Urusan']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_unit' => 'Id Unit',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Nm_Unit' => 'Nm  Unit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefSubUnits()
    {
        return $this->hasMany(RefSubUnit::className(), ['Kd_Urusan' => 'Kd_Urusan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKdUrusan()
    {
        return $this->hasOne(RefBidang::className(), ['Kd_Urusan' => 'Kd_Urusan']);
    }
}
