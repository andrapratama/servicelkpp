<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Ref_Sub_Unit".
 *
 * @property int $Tahun
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property string $Nm_Sub_Unit
 */
class RefSubUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Sub_Unit';
    }

    public static function getDb()
    {
        // use the "db3" application component
        return \Yii::$app->db_main;  
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Nm_Sub_Unit'], 'required'],
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub'], 'integer'],
            [['Nm_Sub_Unit'], 'string'],
            [['Kd_Bidang', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'Tahun'], 'unique', 'targetAttribute' => ['Kd_Bidang', 'Kd_Sub', 'Kd_Unit', 'Kd_Urusan', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'Nm_Sub_Unit' => 'Nm  Sub  Unit',
        ];
    }
}
