<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Ref_Perubahan".
 *
 * @property int $Tahun
 * @property int $Kd_Perubahan
 * @property string $Uraian
 * @property int $Kd_Peraturan
 */
class RefPerubahan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Perubahan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Perubahan', 'Uraian'], 'required'],
            [['Tahun', 'Kd_Perubahan', 'Kd_Peraturan'], 'integer'],
            [['Uraian'], 'string'],
            [['Kd_Perubahan', 'Tahun'], 'unique', 'targetAttribute' => ['Kd_Perubahan', 'Tahun']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Perubahan' => 'Kd  Perubahan',
            'Uraian' => 'Uraian',
            'Kd_Peraturan' => 'Kd  Peraturan',
        ];
    }
}
