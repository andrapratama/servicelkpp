<?php
namespace common\components;

use yii\base\Component;
use backend\models\RefApi;
use linslin\yii2\curl;

class KeyComponent extends Component{
    //public $link;
	
    public function init(){
        parent::init();
    }
	
    public function getTokenPaketKeras(){

        $model = RefApi::find()->where([
            'id_name' => 'pkeras',
            'tipe' => 'auth'
        ])->one();
        if ($model->tgltoken + $model->masatoken < strtotime(date('Y-m-d H:i:s')) || $model->token == null || $model->tgltoken == null || $model->masatoken == null){
            $curl = new curl\Curl();
            $params = json_encode([
                'grant_type'    => 'password',
                'username'      => $model->username,
                'password'          => $model->pwd,
                'client_id'     => 'testclient',
                'client_secret' => 'testpass'
            ]);
            $response = $curl->setRequestBody($params)
                ->setHeaders([
                    'Content-Type' => 'application/json',
                    'Content-Length' => strlen($params)
                ])
                ->post($model->link);
            //var_dump($response);
            $res = json_decode($response);
            $acs = $res->access_token;
            $mst = $res->expires_in;
            $model->token = $acs;
            $model->tgltoken = strtotime(date('Y-m-d H:i:s'));
            $model->masatoken = $mst;
            $model->save();
        }
        return $model->token;
    }
}
?>