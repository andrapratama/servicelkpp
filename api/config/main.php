<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'timeZone' => 'Asia/Jakarta',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-api',
            'parsers'   => [
                'application/json'  => 'yii\web\JsonParser'
            ]
        ],
        'user' => [
            'identityClass' => 'api\models\User',
            'enableAutoLogin' => false,
            'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-api',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['sts', 'lkpp', 'hasil', 'rekening-skpd', 'pkeras','banksumut'],
                    'pluralize' => false,
                    'extraPatterns' => [
                        'GET serviceprogram' => 'program',
                        'GET servicekegiatan' => 'kegiatan',
                        'GET serviceobjekakun' => 'belanja',
                        'GET servicerinciobjekakun' => 'rincian',
                        'GET serviceanggaran' => 'anggaran',
                        'GET serviceppk' => 'ppk',
                        'GET servicepenyedia' => 'penyedia',
                        'GET serviceswakelola' => 'swakelola',
                        'GET servicerevisipaket' => 'revisipaket',
                        'GET test' => 'test',
                        'POST ceknip' => 'cek-nip',
                        'GET referensi' => 'referensi',
                        'POST create' => 'create',
                        'POST update' => 'update-sts',
                        'POST create-kontra' => 'create-kontra',
                        'POST update-kontra' => 'update-kontra-sts',
                        'POST lihat' => 'lihat-sts',
                        'POST tahun' => 'lihat-tahun-sts',
                        'POST tarik-data' => 'tarik-data',
                        'GET token-bsu' => 'get-token'
                    ],
                ],
            ],
        ]

    ],
    'params' => $params,
];
