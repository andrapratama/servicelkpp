<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "Ta_Hasil".
 *
 * @property int $Id
 * @property string $Tahun
 * @property int $Kd_Tahapan
 * @property int $Kd_Peraturan
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property int $Kd_Prog
 * @property int $ID_Prog
 * @property int $Kd_Keg
 * @property int $Kd_Rek_1
 * @property int $Kd_Rek_2
 * @property int $Kd_Rek_3
 * @property int $Kd_Rek_4
 * @property int $Kd_Rek_5
 * @property int $No_Rinc
 * @property int $No_ID
 * @property string $Ket_Prog
 * @property string $Ket_Kegiatan
 * @property string $Lokasi
 * @property string $Kelompok_Sasaran
 * @property string $Status_Kegiatan
 * @property double $Pagu_Anggaran
 * @property double $Pagu_Anggaran_Nt1
 * @property string $Waktu_Pelaksanaan
 * @property int $Kd_Sumber
 * @property int $Kd_Ap_Pub
 * @property string $Keterangan
 * @property string $Sat_1
 * @property string $Nilai_1
 * @property string $Sat_2
 * @property string $Nilai_2
 * @property string $Sat_3
 * @property string $Nilai_3
 * @property string $Satuan123
 * @property string $Jml_Satuan
 * @property string $Nilai_Rp
 * @property string $Total
 * @property string $Asal_Biaya
 * @property string $Uraian_Asal_Biaya
 * @property string $Ref_Usulan_Rincian
 * @property string $DateCreate
 * @property string $Asal_Data
 * @property int $Flat
 * @property string $Token
 */
class TaHasil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    //adding aliases column
    public $pagu;
    public $id_seq_prog;
    public $id_seq_keg;
    public $id_seq_belanja;
    public $id_seq_rinc_belanja;
    public $id_seq_rinc_belanja_sub;
    public $Kd_Unit_1;
    public $Kd_Sub_1;
    public $Ket_Program;
    public $Nm_Rek_5;
    public $CREATED_TIME;
    public $LASTUPDATED_TIME;
    public $BTLP;
    public $BTL;
    public $BLP;
    public $BLBJ;
    public $BLM;
    public $id_unit;
    public $KLDI;

    public static function tableName()
    {
        return 'Ta_Hasil';
    }

    public static function getDb(){
        return Yii::$app->db_dev2;
    }

    public function fields(){
        switch (Yii::$app->controller->action->uniqueId) {
            case 'lkpp/program':
                return $this->lkpp_program();
                break;
            
            case 'lkpp/kegiatan':
                return $this->lkpp_kegiatan();
                break;
            case 'lkpp/belanja':
                return $this->lkpp_belanja();
                break;
            case 'lkpp/rincian':
                return $this->lkpp_rincian();
                break;
            case 'lkpp/anggaran':
                return $this->lkpp_anggaran();
                break;
            default:
                return parent::fields();
                break;
        }
    }

    public function lkpp_program(){
        return [
            'ID_PROGRAM' => function(){
                return $this->id_seq_prog + $this->Kd_Unit;
            },
            'ID_SATKER'  => function(){
                return $this->Kd_Urusan . str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). $this->Kd_Sub;
            },
            'NAMA_PROGRAM' => function(){
                return $this->Ket_Program;
            },
            'KODE_PROGRAM' => 'Kd_Prog',
            'PAGU'  => function(){
                return number_format($this->pagu,0,'','');
            },
            'IS_DELETED' => function(){
                return false;
            },
            'CREATE_TIME' => function(){
                //var_dump($this->CREATED_TIME);exit;
                return strtotime($this->CREATED_TIME);
            },
            'LASTUPDATE_TIME' => function(){
                return strtotime($this->LASTUPDATED_TIME);
            }
        ];
    }

    public function lkpp_kegiatan(){
        return [
            'ID_PROGRAM' => function(){
                return $this->id_seq_prog + $this->Kd_Unit;
            },
            'ID_KEGIATAN' => function(){
                return $this->id_seq_keg;
            },
            'ID_SATKER'  => function(){
                return $this->Kd_Urusan . str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). $this->Kd_Sub;
            },
            'NAMA_KEGIATAN' => 'Ket_Kegiatan',
            'KODE_KEGIATAN' => 'Kd_Keg',
            'PAGU'  => function(){
                return number_format($this->pagu,0,'','');
            },
            'IS_DELETED' => function(){
                return false;
            },
            'CREATE_TIME' => function(){
                //var_dump($this->CREATED_TIME);exit;
                return strtotime($this->CREATED_TIME);
            },
            'LASTUPDATE_TIME' => function(){
                return strtotime($this->LASTUPDATED_TIME);
            }
        ];
    }

    public function lkpp_belanja(){
        return [
            'ID_PROGRAM' => function(){
                return $this->id_seq_prog + $this->Kd_Unit;
            },
            'ID_KEGIATAN' => function(){
                return $this->id_seq_keg;
            },
            'ID_OBJEK_AKUN' => function(){
                return $this->id_seq_belanja;
            },
            'ID_SATKER'  => function(){
                return $this->Kd_Urusan . str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). $this->Kd_Sub;
            },
            'NAMA_OBJEK_AKUN' => function(){
                return $this->Nm_Rek_5;
            },
            'KODE_OBJEK_AKUN' => function(){
                return $this->Kd_Rek_1.$this->Kd_Rek_2.$this->Kd_Rek_3.str_pad($this->Kd_Rek_4, 2, '0', STR_PAD_LEFT) . str_pad($this->Kd_Rek_5, 2, '0', STR_PAD_LEFT);
            },
            'PAGU'  => function(){
                return number_format($this->pagu,0,'','');
            },
            'IS_DELETED' => function(){
                return false;
            },
            'CREATE_TIME' => function(){
                //var_dump($this->CREATED_TIME);exit;
                return strtotime($this->CREATED_TIME);
            },
            'LASTUPDATE_TIME' => function(){
                return strtotime($this->LASTUPDATED_TIME);
            }
        ];
    }

    public function lkpp_rincian(){
        return [
            'ID_PROGRAM' => function(){
                return $this->id_seq_prog + $this->Kd_Unit;
            },
            'ID_KEGIATAN' => function(){
                return $this->id_seq_keg;
            },
            'ID_OBJEK_AKUN' => function(){
                return $this->id_seq_belanja;
            },
            'ID_RINCI_OBJEK_AKUN' => function(){
                return $this->id_seq_rinc_belanja;
            },
            'ID_SATKER'  => function(){
                return $this->Kd_Urusan . str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). $this->Kd_Sub;
            },
            'NAMA_RINCI_OBJEK_AKUN' => function(){
                return $this->Keterangan;
            },
            'KODE_RINCI_OBJEK_AKUN' => function(){
                return $this->Kd_Rek_1.$this->Kd_Rek_2.$this->Kd_Rek_3.str_pad($this->Kd_Rek_4, 2, '0', STR_PAD_LEFT) . str_pad($this->Kd_Rek_5, 2, '0', STR_PAD_LEFT). str_pad($this->No_Rinc, 2, '0', STR_PAD_LEFT);
            },
            'PAGU'  => function(){
                return number_format($this->pagu,0,'','');
            },
            'IS_DELETED' => function(){
                return false;
            },
           'CREATE_TIME' => function(){
                //var_dump($this->CREATED_TIME);exit;
                return strtotime($this->CREATED_TIME);
            },
            'LASTUPDATE_TIME' => function(){
                return strtotime($this->LASTUPDATED_TIME);
            }
        ];
    }

    public function lkpp_anggaran(){
        return [
            'ID' => function(){
                return $this->id_unit;
            },
            'BELANJA_LANGSUNG_PEGAWAI' => function(){
                return number_format($this->BLP,0,'','');
            },
            'BELANJA_LANGSUNG_BUKAN_PEGAWAI' => function(){
                return number_format(0,0,'','');
            },
            'BELANJA_LANGSUNG_BUKAN_PEGAWAI_BARANGJASA' => function(){
                return number_format($this->BLBJ,0,'','');
            },
            'BELANJA_LANGSUNG_BUKAN_PEGAWAI_MODAL' => function(){
                return number_format($this->BLM,0,'','');
            },
            'BELANJA_TIDAK_LANGSUNG_PEGAWAI'  => function(){
                return number_format($this->BTLP,0,'','');
            },
            'BELANJA_TIDAK_LANGSUNG_BUKAN_PEGAWAI' => function(){
                return number_format($this->BTL,0,'','');
            },
            'BELANJA_TIDAK_LANGSUNG_BUKAN_PEGAWAI_BUKAN_PENGADAAN' => function(){
                return number_format(0,0,'','');
            },
            'BELANJA_TIDAK_LANGSUNG_BUKAN_PEGAWAI_PENGADAAN' => function(){
                return number_format(0,0,'','');
            },
            'ID_SATKER'  => function(){
                return $this->Kd_Urusan . str_pad($this->Kd_Bidang, 2, '0', STR_PAD_LEFT) . str_pad($this->Kd_Unit, 2, '0', STR_PAD_LEFT). $this->Kd_Sub;
            },
            'ID_KLDI' => function(){
                return $this->KLDI;
            } 
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id'], 'required'],
            [['Id', 'Kd_Tahapan', 'Kd_Peraturan', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'No_Rinc', 'No_ID', 'Kd_Sumber', 'Kd_Ap_Pub', 'Flat', 'ID_PROGRAM'], 'integer'],
            [['Tahun', 'Ket_Prog', 'Ket_Kegiatan', 'Lokasi', 'Kelompok_Sasaran', 'Status_Kegiatan', 'Waktu_Pelaksanaan', 'Keterangan', 'Sat_1', 'Sat_2', 'Sat_3', 'Satuan123', 'Asal_Biaya', 'Uraian_Asal_Biaya', 'Ref_Usulan_Rincian', 'Asal_Data', 'Token'], 'string'],
            [['Pagu_Anggaran', 'Pagu_Anggaran_Nt1', 'Nilai_1', 'Nilai_2', 'Nilai_3', 'Jml_Satuan', 'Nilai_Rp', 'Total'], 'number'],
            [['DateCreate'], 'safe'],
            [['Id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Tahun' => 'Tahun',
            'Kd_Tahapan' => 'Kd  Tahapan',
            'Kd_Peraturan' => 'Kd  Peraturan',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'Kd_Prog' => 'Kd  Prog',
            'ID_Prog' => 'Id  Prog',
            'Kd_Keg' => 'Kd  Keg',
            'Kd_Rek_1' => 'Kd  Rek 1',
            'Kd_Rek_2' => 'Kd  Rek 2',
            'Kd_Rek_3' => 'Kd  Rek 3',
            'Kd_Rek_4' => 'Kd  Rek 4',
            'Kd_Rek_5' => 'Kd  Rek 5',
            'No_Rinc' => 'No  Rinc',
            'No_ID' => 'No  ID',
            'Ket_Prog' => 'Ket  Prog',
            'Ket_Kegiatan' => 'Ket  Kegiatan',
            'Lokasi' => 'Lokasi',
            'Kelompok_Sasaran' => 'Kelompok  Sasaran',
            'Status_Kegiatan' => 'Status  Kegiatan',
            'Pagu_Anggaran' => 'Pagu  Anggaran',
            'Pagu_Anggaran_Nt1' => 'Pagu  Anggaran  Nt1',
            'Waktu_Pelaksanaan' => 'Waktu  Pelaksanaan',
            'Kd_Sumber' => 'Kd  Sumber',
            'Kd_Ap_Pub' => 'Kd  Ap  Pub',
            'Keterangan' => 'Keterangan',
            'Sat_1' => 'Sat 1',
            'Nilai_1' => 'Nilai 1',
            'Sat_2' => 'Sat 2',
            'Nilai_2' => 'Nilai 2',
            'Sat_3' => 'Sat 3',
            'Nilai_3' => 'Nilai 3',
            'Satuan123' => 'Satuan123',
            'Jml_Satuan' => 'Jml  Satuan',
            'Nilai_Rp' => 'Nilai  Rp',
            'Total' => 'Total',
            'Asal_Biaya' => 'Asal  Biaya',
            'Uraian_Asal_Biaya' => 'Uraian  Asal  Biaya',
            'Ref_Usulan_Rincian' => 'Ref  Usulan  Rincian',
            'DateCreate' => 'Date Create',
            'Asal_Data' => 'Asal  Data',
            'Flat' => 'Flat',
            'Token' => 'Token',
            'ID_PROGRAM' => 'ID_PROGRAM'
        ];
    }

}
