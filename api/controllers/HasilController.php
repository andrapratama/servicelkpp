<?php

namespace api\controllers;

use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

class HasilController extends ActiveController
{
	public $modelClass = 'api\models\TaHasil';

	public function actions()
	{
	    $actions = parent::actions();

	    // disable the "delete" and "create" actions
	    unset($actions['delete'], $actions['create']);

	    // customize the data provider preparation with the "prepareDataProvider()" method
	    $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

	    return $actions;
	}

	public function prepareDataProvider()
    {
    	$params = \Yii::$app->getRequest()->getQueryParams();
    	$model = $this->modelClass;
    	$query = $model::find();
    	if (isset($params['tahapan']))
    		$query->where(['Kd_Tahapan' => $params['tahapan']]);
        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
