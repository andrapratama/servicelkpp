<?php

namespace api\controllers;

use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use backend\models\TaPPK;
use backend\models\TaRASKArsipLog;
use backend\models\TaPenyedia;
use backend\models\TaSwakelola;
use backend\models\TaRevisiPaket;
use backend\models\RefTarikOPD;

class SinkController extends ActiveController
{
    public $modelClass = 'backend\models\TaRASKArsip';

    public function actions() {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update'], $actions['view'], $actions['index']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        //$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                //'only' => ['index', 'view']
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }
   
    public function actionProgram(){
        set_time_limit(300);
        date_default_timezone_set("Asia/Jakarta");
        
        $model = $this->modelClass;
        $params = \Yii::$app->getRequest()->getQueryParams();
        $log = new \backend\models\LogLkpp();
        $log->Jenis = 'Program';
        $log->waktuAkses = date('Y-m-d H:i:s');
        $log->Tahun = isset($params['Tahun']) ? $params['Tahun'] : '2019';
        $log->date = $params['date'];
        $log->dateEnd = $params['dateEnd'];
        $log->save(false);
        $tahun = 2019;
        if (isset($params['tahun']))
            $tahun = $params['tahun'];
        $tglCreate = TaRASKArsipLog::find()->where(['Kd_Perubahan'=>6, 'Tahun'=>$tahun ])->min('Tgl_Posting');
        
        
        $query = $model::findBySql("SELECT *, '".$tglCreate."' CREATED_TIME, '".date('Y-m-d'). ' 00:00:00'."' LASTUPDATED_TIME FROM fn_Lkpp_Program(:Tahun, :D1, :D2)",[
            ':Tahun' => $tahun,
            ':D1' => date('Y-m-d', $params['date']),
            ':D2' => date('Y-m-d', $params['dateEnd'] - 1),
        ]);
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000,
            ],
        ]);

    }

    public function actionKegiatan(){
        set_time_limit(300);
        date_default_timezone_set("Asia/Jakarta");
        
        $model = $this->modelClass;
        $tahun = date('Y');
        $params = \Yii::$app->getRequest()->getQueryParams();
        $log = new \backend\models\LogLkpp();
        $log->Jenis = 'Kegiatan';
        $log->waktuAkses = date('Y-m-d H:i:s');
        $log->Tahun = isset($params['Tahun']) ? $params['Tahun'] : '2019';
        $log->date = $params['date'];
        $log->dateEnd = $params['dateEnd'];
        $log->save(false);
        if (isset($params['tahun']))
            $tahun = $params['tahun'];
        $tglCreate = TaRASKArsipLog::find()->where(['Kd_Perubahan'=>6, 'Tahun'=>$tahun ])->min('Tgl_Posting');
        $query = $model::findBySql("SELECT *, '".$tglCreate."' CREATED_TIME, '".date('Y-m-d'). ' 00:00:00'."' LASTUPDATED_TIME FROM fn_Lkpp_Kegiatan(:Tahun, :D1, :D2)",[
            ':Tahun' => $tahun,
            ':D1' => date('Y-m-d', $params['date']),
            ':D2' => date('Y-m-d', $params['dateEnd'] - 1),
        ]);
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10000,
            ],
        ]);

    }

    public function actionBelanja(){
        set_time_limit(300);
        ini_set('memory_limit', '256M');
        date_default_timezone_set("Asia/Jakarta");
        
        $model = $this->modelClass;
        $params = \Yii::$app->getRequest()->getQueryParams();
        $log = new \backend\models\LogLkpp();
        $log->Jenis = 'Belanja';
        $log->waktuAkses = date('Y-m-d H:i:s');
        $log->Tahun = isset($params['Tahun']) ? $params['Tahun'] : '2019';
        $log->date = $params['date'];
        $log->dateEnd = $params['dateEnd'];
        $log->save(false);
        $tahun = date('Y');
        if (isset($params['tahun']))
            $tahun = $params['tahun'];
        $tglCreate = TaRASKArsipLog::find()->where(['Kd_Perubahan'=>6, 'Tahun'=>$tahun ])->min('Tgl_Posting');
        $query = $query = $model::findBySql("SELECT *, '".$tglCreate."' CREATED_TIME, '".date('Y-m-d'). ' 00:00:00'."' LASTUPDATED_TIME FROM fn_Lkpp_Belanja(:Tahun, :D1, :D2)",[
            ':Tahun' => $tahun,
            ':D1' => date('Y-m-d', $params['date']),
            ':D2' => date('Y-m-d', $params['dateEnd'] - 1),
        ]);
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 100000,
            ],
        ]);

    }

    public function actionRincian(){
        set_time_limit(300);
        ini_set('memory_limit', '512M');
        date_default_timezone_set("Asia/Jakarta");
       
        $model = $this->modelClass;
        $params = \Yii::$app->getRequest()->getQueryParams();
        $log = new \backend\models\LogLkpp();
        $log->Jenis = 'Rincian';
        $log->waktuAkses = date('Y-m-d H:i:s');
        $log->Tahun = isset($params['Tahun']) ? $params['Tahun'] : '2019';
        $log->date = $params['date'];
        $log->dateEnd = $params['dateEnd'];
        $log->save(false);
        $tahun = date('Y');
        if (isset($params['tahun']))
            $tahun = $params['tahun'];
        $tglCreate = TaRASKArsipLog::find()->where(['Kd_Perubahan'=>6, 'Tahun'=>$tahun ])->min('Tgl_Posting');
        $query = $model::findBySql("SELECT *, '".$tglCreate."' CREATED_TIME, '".date('Y-m-d'). ' 00:00:00'."' LASTUPDATED_TIME FROM fn_Lkpp_Belanja_Rinc(:Tahun, :D1, :D2)",[
            ':Tahun' => $tahun,
            ':D1' => date('Y-m-d', $params['date']),
            ':D2' => date('Y-m-d', $params['dateEnd'] - 1),
        ]);
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 100000,
            ],
        ]);

    }

    public function actionPpk(){
        set_time_limit(300);
        date_default_timezone_set("Asia/Jakarta");
        
        $params = \Yii::$app->getRequest()->getQueryParams();
        $log = new \backend\models\LogLkpp();
        $log->Jenis = 'Ppk';
        $log->waktuAkses = date('Y-m-d H:i:s');
        $log->Tahun = isset($params['Tahun']) ? $params['Tahun'] : '2019';
        $log->date = $params['date'];
        $log->dateEnd = $params['dateEnd'];
        $log->save(false);
        $tahun = isset($params['Tahun']) ? $params['Tahun'] : '2019';
        $query = TaPPK::findBySql('SELECT * FROM fn_Lkpp_Ppk(:Tahun, :D1, :D2)',[
            ':Tahun' => $tahun,
            ':D1' => date('Y-m-d', $params['date']),
            ':D2' => date('Y-m-d', $params['dateEnd'] - 1),
        ]);
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10000,
            ],
        ]);
    }

    public function actionAnggaran(){
        set_time_limit(300);
        date_default_timezone_set("Asia/Jakarta");
        $params = \Yii::$app->getRequest()->getQueryParams();
        $log = new \backend\models\LogLkpp();
        $log->Jenis = 'Anggaran';
        $log->waktuAkses = date('Y-m-d H:i:s');
        $log->Tahun = isset($params['Tahun']) ? $params['Tahun'] : '2019';
        $log->date = $params['date'];
        $log->dateEnd = $params['dateEnd'];
        $log->save(false);
        $model = $this->modelClass;
        $query = $model::find()->select([
            'BTLP' => new Expression('SUM(CASE WHEN Kd_Rek_1 = 5 AND Kd_Rek_2 = 1 AND Kd_Rek_3 = 1 AND Kd_Rek_4 = 1 THEN Total ELSE 0 END)'),
            'BTL' => new Expression('SUM(CASE WHEN Kd_Rek_1 = 5 AND Kd_Rek_2 = 1 AND NOT(Kd_Rek_3 = 1 AND Kd_Rek_4 = 1) THEN Total ELSE 0 END)'),
            'BLP' => new Expression('SUM(CASE WHEN Kd_Rek_1 = 5 AND Kd_Rek_2 = 2 AND Kd_Rek_3 = 1 THEN Total ELSE 0 END)'),
            'BLBJ' => new Expression('SUM(CASE WHEN Kd_Rek_1 = 5 AND Kd_Rek_2 = 2 AND Kd_Rek_3 = 2 THEN Total ELSE 0 END)'),
            'BLM' => new Expression('SUM(CASE WHEN Kd_Rek_1 = 5 AND Kd_Rek_2 = 2 AND Kd_Rek_3 = 3 THEN Total ELSE 0 END)'),
            'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub',
            'id_unit' => new Expression('ROW_NUMBER() OVER(ORDER BY Kd_Urusan, Kd_Bidang, Kd_Unit, Kd_Sub)'),
            'KLDI' => new Expression("'D498'")
        ]);
        $query->where(['Kd_Perubahan' => 6]);
        $query->groupBy('Kd_Urusan, Kd_Bidang, Kd_Unit, Kd_Sub');
        if (isset($params['tahun']))
            $query->andWhere(['=', 'Ta_RASK_Arsip.Tahun', $params['tahun']]);
        else
            $query->andWhere(['=', 'Ta_RASK_Arsip.Tahun', '2019']);
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10000,
            ],
        ]);
    }

    public function actionPenyedia(){
        set_time_limit(300);
        date_default_timezone_set("Asia/Jakarta");
        $params = \Yii::$app->getRequest()->getQueryParams();
        $log = new \backend\models\LogLkpp();
        $log->Jenis = 'Penyedia';
        $log->waktuAkses = date('Y-m-d H:i:s');
        $log->Tahun = isset($params['Tahun']) ? $params['Tahun'] : '2019';
        $log->date = $params['date'];
        $log->dateEnd = $params['dateEnd'];
        $log->save(false);
        /*echo date('Y-m-d H:i:s',$params['date']);
        echo '<br>';
        echo date('Y-m-d H:i:s',$params['dateEnd']);*/
        $tarik = RefTarikOPD::find()->select('*')->where(['Tahun'=>'2019'])->one();
        $query = TaPenyedia::find();
        if (isset($params['id']))
            $query->where(['ID_RUP' => $params['id']]);
        if (isset($params['date']))
            $query->andWhere(['>=', 'Lastupdate_Time',  date('Y-m-d H:i:s',$params['date'])]);
        if (isset($params['dateEnd']))
            $query->andWhere(['<=', 'Lastupdate_Time',  date('Y-m-d H:i:s',$params['dateEnd'])]);
        if (isset($params['tahun']))
            $query->andWhere(['=', 'Tahun', $params['tahun']]);
        else
            $query->andWhere(['=', 'Tahun', '2019']);
        $query->andWhere(['Kd_Urusan' => $tarik->Kd_Urusan, 'Kd_Bidang' => $tarik->Kd_Bidang, 'Kd_Unit' => $tarik->Kd_Unit, 'Kd_Sub' => $tarik->Kd_Sub]);
       //$query->andWhere(['ID_RUP' => [23038]]);
        //$query->andWhere(['ID_RUP' => [22850,22851,22852,22853,22854]]);
      // $query->andWhere(['=', 'ID_RUP', 22934]);
       //$query->andWhere(['>', 'Total_Pagu', 22692700]);
       //$query->andWhere(['<', 'Total_Pagu', 45016340]);
        //IN(355, 356,357, 358, 359,360,361,362,363)
       // $query->andWhere(['>=', 'ID_RUP', 4148]);
        //$query->andWhere(['Lastupdate_Time' => '2019-06-26 00:43:49']);        
        $query->andWhere(['Umumkan' => 1]);
       $query->andWhere(['Aktif' => 1, 'Is_Deleted' => 0]);
        // //var_dump($query->createCommand()->getRawSql());exit;
        //var_dump($tarik->Jn_Tarik);exit();
        if ($tarik->Jn_Tarik=='P') {
            return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10000,
            ],
                ]);
        } else {
          return []; 
        }
    }

    public function actionSwakelola(){
        set_time_limit(300);
        date_default_timezone_set("Asia/Jakarta");
        $params = \Yii::$app->getRequest()->getQueryParams();
        $log = new \backend\models\LogLkpp();
        $log->Jenis = 'Swakelola';
        $log->waktuAkses = date('Y-m-d H:i:s');
        $log->Tahun = isset($params['Tahun']) ? $params['Tahun'] : '2019';
        $log->date = $params['date'];
        $log->dateEnd = $params['dateEnd'];
        $log->save(false);
        $tahun = isset($params['Tahun']) ? $params['Tahun'] : '2019';
        $query = TaSwakelola::findBySql("SELECT * FROM fn_Lkpp_Swakelola(:Tahun, :D1, :D2)",[
            ':Tahun' => $tahun,
            ':D1' => date('Y-m-d', $params['date']),
            ':D2' => date('Y-m-d', $params['dateEnd'] - 1),
        ]);
        //$query->andWhere(['=', 'ID_RUP', 13576]);
        //return [];
        $tarik = RefTarikOPD::find()->select('*')->where(['Tahun'=>'2019'])->one();
        if ($tarik->Jn_Tarik=='S') {
        return new ActiveDataProvider([
             'query' => $query,
             'pagination' => [
                 'pageSize' => 10000,
             ],
         ]);
        } else {
           return []; 
        }
    }

    public function actionRevisipaket(){
        set_time_limit(300);
        date_default_timezone_set("Asia/Jakarta");
        $params = \Yii::$app->getRequest()->getQueryParams();
        $log = new \backend\models\LogLkpp();
        $log->Jenis = 'RevisiPaket';
        $log->waktuAkses = date('Y-m-d H:i:s');
        $log->Tahun = isset($params['Tahun']) ? $params['Tahun'] : '2019';
        $log->date = $params['date'];
        $log->dateEnd = $params['dateEnd'];
        $log->save(false);
        $query = TaRevisiPaket::find()->select(['ID', 'ID_RUP', 'ID_RUP_TO', 'JENIS', 'TIPE', 'ALASAN_REVISI', 'CREATE_TIME']);
        if (isset($params['id']))
            $query->where(['ID_RUP' => $params['id']]);
        if (isset($params['date']))
            $query->andWhere(['>=', 'Create_Time',  date('Y-m-d H:i:s',$params['date'])]);
        if (isset($params['dateEnd']))
            $query->andWhere(['<=', 'Create_Time',  date('Y-m-d H:i:s',$params['dateEnd'])]);
        if (isset($params['tahun']))
            $query->andWhere(['=', 'Tahun', $params['tahun']]);
        else
            $query->andWhere(['=', 'Tahun', '2019']);
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10000,
            ],
        ]);
    }

}
