<?php

namespace api\controllers;

use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class PkerasController extends ActiveController
{
    public $modelClass = 'backend\models\RefUrusan';

    public function actions() {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update'], $actions['view'], $actions['index']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        //$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }
   
    public function actionReferensi($id, $tahun = null){
        if ($tahun == NULL)
            $tahun = date('Y');
        $model = 'backend\models\Ref';
        $namas = explode('-', $id);
        foreach ($namas as $key => $nama) {
             $model .= ucfirst($nama);
        }  
        $field = new $model([]);
        $fieldsAwal = $field->attributes;
        unset($fieldsAwal['Tahun']);
        $fields = array_keys($fieldsAwal);
        try{
            $query = $model::find()->select($fields)->where(['Tahun' => $tahun]);
        }catch(yii\base\ErrorException $e){
            throw new yii\web\NotFoundHttpException();
        }
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000,
            ],
        ]);
    }

    public function actionCekNip(){
        $query = \backend\models\RefVerifikasiAsnEceknya::find()->where([
            'Nip' => \Yii::$app->request->getBodyParam('NIP')
        ])->asArray()->one();
        if ($query == NULL)
            return ['RC' => '91', 'message' => 'Data tidak ditemukan'];
        $query['RC'] = '00';
        return $query;

    }

    
}
