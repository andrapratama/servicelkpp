<?php

namespace api\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use common\models\RefRek4;
use common\models\RefRek5;

class StsController extends ActiveController
{
	var $_id;
    var $_nilai;
    var $_denda;
    var $_tgl;
    var $_masa;
    var $_tempo;
    var $_ketetapan;
    var $_mataanggaran;
    var $_model;
    var $_kdmasuk;
    var $_kd_unit;
    var $_STS;
	public $modelClass = 'api\models\STSHistory';

	public function actions() {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update'], $actions['view'], $actions['index']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        //$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actionLihatSts(){
        $model = $this->modelClass;
        if (strlen(\Yii::$app->getRequest()->getBodyParam('No_STS')) === 20){
            $str = \Yii::$app->getRequest()->getBodyParam('No_STS');
            $tahun = (int)substr($str, 18) > 20 ? '19'.substr($str, 18) : '20'.substr($str, 18);
            return $model::find()
                            ->where(['No_NOP' => substr($str, 0, 18)])
                            ->andWhere(['like', 'masa_bayar', $tahun])
                            //->andWhere(['masa_bayar' => \Yii::$app->getRequest()->getBodyParam('Tahun')])
                            ->one();
        }
        return $model::find()->where(['No_STS' => \Yii::$app->getRequest()->getBodyParam('No_STS')])
                            ->orWhere(['No_NOP' => \Yii::$app->getRequest()->getBodyParam('No_STS')])
                            ->orWhere(['No_Pokok_WP' => \Yii::$app->getRequest()->getBodyParam('No_STS')])
                            ->orWhere(['No_Pokok_WR' => \Yii::$app->getRequest()->getBodyParam('No_STS')])
                            //->andWhere(['masa_bayar' => \Yii::$app->getRequest()->getBodyParam('Tahun')])
                            ->one();
    }

    public function actionLihatTahunSts(){
        $model = $this->modelClass;
        if (strlen(\Yii::$app->getRequest()->getBodyParam('No_STS')) === 20){
            $str = \Yii::$app->getRequest()->getBodyParam('No_STS');
            $tahun = (int)substr($str, 18) > 20 ? '19'.substr($str, 18) : '20'.substr($str, 18);
            return $model::find()->select('masa_bayar, Status_Bayar')
                            ->where(['No_NOP' => substr($str, 0, 18)])
                            ->andWhere(['like', 'masa_bayar', $tahun])
                            ->all();
        }
        return $model::find()->select('masa_bayar, Status_Bayar')
                            ->where(['No_STS' => \Yii::$app->getRequest()->getBodyParam('No_STS')])
                            ->orWhere(['No_NOP' => \Yii::$app->getRequest()->getBodyParam('No_STS')])
                            ->orWhere(['No_Pokok_WP' => \Yii::$app->getRequest()->getBodyParam('No_STS')])
                            ->orWhere(['No_Pokok_WR' => \Yii::$app->getRequest()->getBodyParam('No_STS')])
                            ->all();
    }

    public function actionCreate(){
    	set_time_limit(60);
        $model = new $this->modelClass([]);
        $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->Kd_Unit == NULL)
            $model->Kd_Unit = \Yii::$app->user->identity->Kd_Unit;
        //var_dump(\Yii::$app->getRequest()->getBodyParams());exit;
        if ($this->checkMandatory($model))
        	return ["Message" => 'STS tidak bisa dibuat karena data yang dikirim kurang', "responseCode" => '14'];
        if (!$this->checkIdentitas($model))
        	return ["Message" => $model, "responseCode" => '14'];
        $model->Kd_Tarik = 0;
        $this->_kd_unit = $model->Kd_Unit;
        //$model->Tahun = 2019;
        $model->access_token = \Yii::$app->user->identity->access_token;
        if ($model->validate() && $model->save()) {
        	$modelSTS = $this->modelClass;
            $this->_kdmasuk = $model->Kd_Masuk;
            $count = 0;
            while (($this->_STS = $modelSTS::findOne(['Kd_Masuk' => $this->_kdmasuk])->No_STS) == null && $count < 10) {
                sleep(3);
                $count += 1;
            }
            if ($this->_STS != null) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(201);
                $TglSTS = $modelSTS::findOne(['No_STS' => $this->_STS])->Tgl_STS;
                return ["Message" => 'STS berhasil dibuat', "responseCode" => "00", "No_STS" =>  $this->_STS, 'Tgl_STS' => $TglSTS];
            } else {
                return ["Message" => "Nomor STS tidak bisa dibuat, silakan hubungi administrator", "responseCode" => "91"];
            }
        } else {
            return ["Message" => $model->getErrors(), "responseCode" => "91"];
        }
    }

    public function actionCreateKontra(){
        set_time_limit(60);
        $model = new $this->modelClass([]);
        $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->Kd_Unit == NULL)
            $model->Kd_Unit = \Yii::$app->user->identity->Kd_Unit;
        if ($this->checkMandatory($model))
            return ["Message" => 'STS tidak bisa dibuat karena data yang dikirim kurang', "responseCode" => '14'];
        $model->Mata_Anggaran = $this->mergeAnggaran($model->Mata_Anggaran);
        $this->_mataanggaran = $model->Mata_Anggaran;
        $model->Kd_Tarik = 0;
        $this->_kd_unit = $model->Kd_Unit;
        $model->access_token = \Yii::$app->user->identity->access_token;
        if ($model->validate() && $model->save()) {
            $modelSTS = $this->modelClass;
            $this->_kdmasuk = $model->Kd_Masuk;
            $count = 0;
            while (($this->_STS = $modelSTS::findOne(['Kd_Masuk' => $this->_kdmasuk])->No_STS) == null && $count < 10) {
                sleep(3);
                $count += 1;
            }
            if ($this->_STS != null) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(201);
                $TglSTS = $modelSTS::findOne(['No_STS' => $this->_STS])->Tgl_STS;
                return ["Message" => 'STS berhasil dibuat', "responseCode" => "00", "No_STS" =>  $this->_STS, 'Tgl_STS' => $TglSTS];
            } else {
                return ["Message" => "Nomor STS tidak bisa dibuat, silakan hubungi administrator", "responseCode" => "91"];
            }
        } else {
            return ["Message" => $model->getErrors(), "responseCode" => "91"];
        }
    }

    public function actionUpdateKontraSts() {
        set_time_limit(60);
        $mod = $this->modelClass;
        $model = $mod::findOne([
            'No_STS' => \Yii::$app->getRequest()->getBodyParam('No_STS'),
            'access_token' => \Yii::$app->user->identity->access_token
        ]);
        if ($model === NULL){
            return ["Message" => 'Nomor STS tidak ditemukan atau anda tidak diperbolehkan untuk update STS ini, silakan periksa Nomor STS anda.', "responseCode" => '14'];
        }
        $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if ($this->checkMandatory($model))
            return ["Message" => 'STS tidak bisa diupdate karena data yang dikirim kurang', "responseCode" => '14'];
        $model->Mata_Anggaran = $this->mergeAnggaran($model->Mata_Anggaran);
        $this->_mataanggaran = $model->Mata_Anggaran;
        if ($model->validate() && $model->save()) {
            return ["Message" => 'Data dengan Nomor STS '.$model->No_STS.' berhasil diperbaharui', "responseCode" => "00"];
        } else {
            return ["Message" => $model->getErrors(), "responseCode" => "91"];
        }
    }

    public function actionUpdateSts() {
        set_time_limit(60);
        $mod = $this->modelClass;
        $model = $mod::findOne([
            'No_STS' => \Yii::$app->getRequest()->getBodyParam('No_STS'),
            'access_token' => \Yii::$app->user->identity->access_token
        ]);
        if ($model === NULL){
            return ["Message" => \Yii::$app->user->identity->access_token, "responseCode" => '14'];
        }
        $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if ($this->checkMandatory($model))
            return ["Message" => 'STS tidak bisa diupdate karena data yang dikirim kurang', "responseCode" => '14'];
        if (!$this->checkIdentitas($model))
            return ["Message" => 'STS tidak bisa diupdate karena data kode rekening atau penempatan field rekening salah!', "responseCode" => '14'];
        if ($model->validate() && $model->save()) {
            return ["Message" => 'Data dengan Nomor STS '.$model->No_STS.' berhasil diperbaharui', "responseCode" => "00"];
        } else {
            return ["Message" => $model->getErrors(), "responseCode" => "91"];
        }
    }

    private function checkMandatory($model){
    	return $model->Nilai === null || $model->Denda === null || $model->Mata_Anggaran === null || $model->masa_bayar === null || $model->jatuh_tempo === null;
    }

    private function checkIdentitas(&$model){
    	$Mata_Anggaran = explode('.', $model->Mata_Anggaran);
        if (count($Mata_Anggaran) == 1 && strlen($Mata_Anggaran[0]) == 9){
            $this->_mataanggaran = $model->Mata_Anggaran;
            return true;
        } 
    	if ((int)$Mata_Anggaran[1] == 1 && (int)$Mata_Anggaran[2] == 2){
    		if ($model->No_Pokok_WR == null || $model->Tgl_STRD == null || $model->No_STRD == null)
    			return false;
    		$this->_id = $model->No_Pokok_WR;
	        $this->_ketetapan = $model->No_STRD;
	        $this->_tgl = $model->Tgl_STRD;
	        if (!$this->checkAnggaran($model))
	        	return false;
	        $model->Jn_Retribusi = $this->checkAnggaran($model);
    	}else{
    		if ($model->No_Pokok_WP == null || $model->Tgl_STPD == null || $model->No_STPD == null)
    			return false;
    		$this->_id = $model->No_Pokok_WP;
	        $this->_ketetapan = $model->No_STPD;
	        $this->_tgl = $model->Tgl_STPD;
	        if (!$this->checkAnggaran($model))
	        	return false;
	        $model->Jn_Pajak = $this->checkAnggaran($model);
    	}
    	$Mata_Anggaran[3] = str_pad($Mata_Anggaran[3], 3, "0", STR_PAD_LEFT);
        $Mata_Anggaran[4] = str_pad($Mata_Anggaran[4], 3, "0", STR_PAD_LEFT);
        $model->Mata_Anggaran = $Mata_Anggaran[0] . $Mata_Anggaran[1] . $Mata_Anggaran[2] . $Mata_Anggaran[3] . $Mata_Anggaran[4];
        $this->_mataanggaran = $model->Mata_Anggaran;
    	return true;	
    }

    private function mergeAnggaran($mataAnggaran){
        $Mata_Anggaran = explode('.', $mataAnggaran);
        $Mata_Anggaran[3] = str_pad($Mata_Anggaran[3], 3, "0", STR_PAD_LEFT);
        $Mata_Anggaran[4] = str_pad($Mata_Anggaran[4], 3, "0", STR_PAD_LEFT);
        return $Mata_Anggaran[0] . $Mata_Anggaran[1] . $Mata_Anggaran[2] . $Mata_Anggaran[3] . $Mata_Anggaran[4];
    }

    private function checkAnggaran(&$model){
    	$Mata_Anggaran = explode('.', $model->Mata_Anggaran);
    	if ((int)$Mata_Anggaran[4] == 0){
    		if (($Rekening = RefRek4::findOne(['Kd_Rek_1' => $Mata_Anggaran[0],'Kd_Rek_2' => $Mata_Anggaran[1],'Kd_Rek_3' => $Mata_Anggaran[2],'Kd_Rek_4' => $Mata_Anggaran[3]])) == null)
    			return false;
    		return $Rekening->Nm_Rek_4;
    	}
    	if (($Rekening = RefRek5::findOne(['Kd_Rek_1' => $Mata_Anggaran[0],'Kd_Rek_2' => $Mata_Anggaran[1],'Kd_Rek_3' => $Mata_Anggaran[2],'Kd_Rek_4' => $Mata_Anggaran[3], 'Kd_Rek_5' => $Mata_Anggaran[4]])) == null)
    		return false;
    	return $Rekening->Nm_Rek_5;

    }
}
