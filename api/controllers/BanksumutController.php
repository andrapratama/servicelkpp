<?php

namespace api\controllers;

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use linslin\yii2\curl;

class BanksumutController extends Controller
{
    //public $modelClass = 'backend\models\TaRASKArsip';

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        //'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionGetToken(){
    	set_time_limit(300);
        $curl = new curl\Curl();
        $params = json_encode([
                'grant_type' => 'client_credentials',
                'client_id' => '11',
                'client_secret' => '8KS3sDJdWYbc0vLlocIGCu+JRVvIMosdantmFvXotKE='
         ]);
        //echo $params;
        $response = $curl->setRequestBody($params)
             ->setHeaders([
                'Content-Type' => 'application/json',
                'Content-Length' => strlen($params)
             ])
             ->post('http://182.23.79.68/banking/public/oauth/token');
        var_dump($curl);
    }

    

}
